import React from 'react';

const SPACE_SPAN = <span>{" "}</span>;

function getExpandableStr(originalStr, keyWord=null, limit=19) {
  let expandedStr = originalStr;
  let shortStr = originalStr;
  if (shortStr.length > limit) {
    shortStr = shortStr.substring(0, limit + 1) + "...";
  }
  if (keyWord) {
  	expandedStr = getHighlightedDiv(expandedStr, keyWord);
  	shortStr = getHighlightedDiv(shortStr, keyWord);
  }
  return {short: shortStr, expanded: expandedStr};
}

function getUrlParamStr(params) {
	let urlStr = ""
	for (const key in params) {
		if(urlStr !== "") {
			urlStr += "&";
		}
		let val = params[key];
		if (Array.isArray(val)) {
			val = val.join(',');
		} else {
			val = String(val);
		}
		if (val.length) {
			urlStr += `${key}=${encodeURIComponent(val)}`;
		}
	}
	return urlStr;
}

function hasMatch(word1) {
	return (word2) => {
		return word1.includes(word2);
	};
}

function getHighlightedDiv(str, keyWord) {
	const keyWords = decodeURIComponent(keyWord).toLowerCase().split(/\s/g);
	const words = str.split(/\s/g);
	const highlightedWords = [];
	for (const word of words) {
		const wordLower = word.toLowerCase();
		if (keyWords.some(hasMatch(wordLower))) {
			highlightedWords.push(
				<span style={{backgroundColor: 'yellow'}}>
					{word}
				</span>);
		} else {
			highlightedWords.push(<span>{word}</span>);
		}
		highlightedWords.push(SPACE_SPAN);
	}
	return <div>{highlightedWords}</div>;
}

export { getExpandableStr, getUrlParamStr, getHighlightedDiv };
