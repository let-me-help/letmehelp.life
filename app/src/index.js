import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import LetMeHelpApp from './components/main/LetMeHelpApp';

ReactDOM.render((
  <BrowserRouter>
    <LetMeHelpApp />
  </BrowserRouter>
), document.getElementById('root'));
