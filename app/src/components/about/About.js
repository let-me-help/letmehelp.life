import React, { Component } from 'react';
import orgHunterLogo from './orghunter-logo.png';
import axios from 'axios';
import BreadCrumb from '../general/BreadCrumb';
import Card from '../general/Card';
import ImageCard from '../general/ImageCard';
import Row from '../general/Row';
import { STYLES } from '../../tools/constants';
import logo from '../../logo.png';

const STATS_URL = "https://tmvhaacw7f.execute-api.us-west-2.amazonaws.com/Prod/stats";

const CENTER_STYLE = {
  textAlign: "center",
  marginTop: "10%",
  fontSize: "18pt"
};
const TOP_STYLE = {
  textAlign: "center",
  marginTop: "4%",
  fontSize: "13pt"
};
const MEMBERS = ["patrick", "jorge", "rahul", "rinchen", "daniel"];

function getMembersDict() {
  const dict = {};
  for (const name of MEMBERS) {
    dict[name] = {
      email: "",
      image: null,
      issues: 0,
      commits: 0,
      'unit-tests': 0
    };
  }
  return dict;
}

function emailTag(email) {
  return <a href={"mailto:" + email}>{email}</a>;
}

class About extends Component {

  constructor(props) {
    super(props);
    this.state = {stats: getMembersDict()};
  }

  componentWillMount() {
    axios.get(STATS_URL).then((response) =>
      {
        this.setState({stats: response.data});
      }
    );
  }

  render() {
    return (
    <div className="container">
      <br/><br/>
      <BreadCrumb crumbs={["Home", "About"]}
                  links={["/"]} />
      <Row columnType="col-lg-6">
        <img
          className="mx-auto d-block"
          src={logo}
          alt="LetMeHelp logo"
          style={STYLES.FIT_IMG_STYLE}></img>
        <div>
          <h2>About LetMeHelp</h2>
          <p>LetMeHelp is a non-profit website dedicated to providing charities the recognition they deserve. </p>
          <p>LetMeHelp has various event postings to help you find a local charity event.
           We aim to provide you with an easy way to get connected with local community organizers and help out!</p>
        </div>
      </Row>
      <hr/>
      <h2>Our Team</h2>
      <Row columnType="col-sm-4 mb-3">
        <ImageCard image={this.state.stats.jorge.image}
                   imageTitle="Jorge Hernandez"
                   imageSubtitle="Fullstack Nethead"
                   body={[<p>Senior majoring in Computer Science and Math who enjoys collecting tunes. His favorite show is People Just Do Nothing.</p>,]}
                   footer={emailTag(this.state.stats.jorge.email)}/>
        <ImageCard image={this.state.stats.patrick.image}
                   imageTitle="Patrick Klingler"
                   imageSubtitle="Fullstack Technomancer"
                   body={[<p>Senior majoring in Computer Science. His favorite band is Basic Channel.</p>,]}
                   footer={emailTag(this.state.stats.patrick.email)}/>
        <ImageCard image={this.state.stats.daniel.image}
                 imageTitle="Daniel Loera"
                 imageSubtitle="Fullstack Developer"
                 body={[<p>Senior majoring in computer science. He loves Nintendo, Lucifer (his pet bearded dragon), and Pancho (his pet cat).</p>,]}
                 footer={emailTag(this.state.stats.daniel.email)}/>
        <ImageCard image={this.state.stats.rahul.image}
                 imageTitle="Rahul Parikh"
                 imageSubtitle="Cloud & Backend Developer"
                 body={[<p>Senior majoring in computer science and minoring in Business Foundation and Economics. He loves traveling, badminton and GoT.</p>,]}
                 footer={emailTag(this.state.stats.rahul.email)}/>
        <ImageCard image={this.state.stats.rinchen.image}
                 imageTitle="Rinchen Tsering"
                 imageSubtitle="Fullstack Developer"
                 body={[<p>Senior studying Computer Science and Business at The University of Texas at Austin who enjoys playing soccer, traveling places and helping people.</p>,]}
                 footer={emailTag(this.state.stats.rinchen.email)}/>
      </Row>
      <hr/>
      <h2>Project Details</h2>
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Commits</th>
            <th scope="col">Issues</th>
            <th scope="col">Unit Tests</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Jorge Hernandez</th>
            <td>{this.state.stats.jorge.commits}</td>
            <td>{this.state.stats.jorge.issues}</td>
            <td>{this.state.stats.jorge["unit-tests"]}</td>
          </tr>
          <tr>
            <th scope="row">Patrick Klingler</th>
            <td>{this.state.stats.patrick.commits}</td>
            <td>{this.state.stats.patrick.issues}</td>
            <td>{this.state.stats.patrick["unit-tests"]}</td>
          </tr>
          <tr>
            <th scope="row">Daniel Loera</th>
            <td>{this.state.stats.daniel.commits}</td>
            <td>{this.state.stats.daniel.issues}</td>
            <td>{this.state.stats.daniel["unit-tests"]}</td>
          </tr>
          <tr>
            <th scope="row">Rahul Parikh</th>
            <td>{this.state.stats.rahul.commits}</td>
            <td>{this.state.stats.rahul.issues}</td>
            <td>{this.state.stats.rahul["unit-tests"]}</td>
          </tr>
          <tr>
            <th scope="row">Rinchen Tsering</th>
            <td>{this.state.stats.rinchen.commits}</td>
            <td>{this.state.stats.rinchen.issues}</td>
            <td>{this.state.stats.rinchen["unit-tests"]}</td>
          </tr>
          <tr>
            <th scope="row">Total</th>
            <td>{this.state.stats["total-commits"]}</td>
            <td>{this.state.stats["total-issues"]}</td>
            <td>{this.state.stats["total-unit-tests"]}</td>
          </tr>
        </tbody>
      </table>
      <Row columnType="col-sm-4 mb-4 card-group" >
        <Card header="Important Links"
              body={
                <div style={CENTER_STYLE}>
                  <a href="https://documenter.getpostman.com/view/5451526/RWgm3gcV">API Design(Postman)</a><br/>
                  <a href="https://gitlab.com/let-me-help/letmehelp.life">Gitlab Repository</a>
                </div>}/>
        <Card header="Data Sources/APIs"
              body={
                <div style={TOP_STYLE}>
                  <a href="https://docs.gitlab.com/ee/api/">Gitlab API for project and member statistics</a><br/>
                  <a href="https://www.mediawiki.org/wiki/API:Main_page">Wikipedia API for city descriptions</a><br/>
                  <a href="https://orghunter.3scale.net/">Orghunter API for charities</a><br/>
                  <a href="https://www.eventbrite.com/developer/v3/">Eventbrite API for events data</a><br/>
                  <a href="https://datausa.io/about/api/">Data USA API for location statistics</a>
                </div>}/>
        <Card header="How each API is used"
              body={[<p>The charity, location, and events statistics were all scraped the same way using Postman and copying the data to their respective pages.</p>,
                     <p>The Gitlab stats are scraped and populated via a vanilla JS script included with this page</p>]}/>
      </Row>
      <Card header="Tools"
            body=
              {[
                <h5>API Designed with Postman</h5>,
                 <h5>Bootstrap 4 for frontend styling, specifically this theme: <a href="https://startbootstrap.com/template-overviews/modern-business/">Modern Business Theme</a></h5>,
                 <h5>Google Maps API for map embedding</h5>,
                 <h5>ReactJS</h5>,
                 <h5><b>Charity Search powered by:</b></h5>,
                 <img
                  alt="orghunter logo"
                  src={orgHunterLogo}
                  style={STYLES.FIT_IMG_STYLE}>
                </img>
              ]}
      />
    </div>);
  }
}

export default About;
