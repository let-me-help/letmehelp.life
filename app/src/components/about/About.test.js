import React from 'react';
import jest from 'jest';
import { shallow } from 'enzyme';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import About from './About';

const TEST_RESPONSE = {
  "daniel": {
    "email": "danny.reatret@gmail.com",
    "image": "https:\/\/assets.gitlab-static.net\/uploads\/-\/system\/user\/avatar\/1131707\/avatar.png",
    "issues": 25,
    "commits": 55,
    "unit-tests": 0
  },
  "jorge": {
    "email": "jm.hernandez@utexas.edu",
    "image": "https:\/\/assets.gitlab-static.net\/uploads\/-\/system\/user\/avatar\/2773231\/avatar.png",
    "issues": 23,
    "commits": 33,
    "unit-tests": 0
  },
  "patrick": {
    "email": "patrick1klingler@gmail.com",
    "image": "https:\/\/assets.gitlab-static.net\/uploads\/-\/system\/user\/avatar\/2748996\/avatar.png",
    "issues": 11,
    "commits": 12,
    "unit-tests": 0
  },
  "rahul": {
    "email": "rahulmparikh@gmail.com",
    "image": "https:\/\/assets.gitlab-static.net\/uploads\/-\/system\/user\/avatar\/2378417\/avatar.png",
    "issues": 8,
    "commits": 9,
    "unit-tests": 0
  },
  "rinchen": {
    "email": "rinchen4433@gmail.com",
    "image": "https:\/\/assets.gitlab-static.net\/uploads\/-\/system\/user\/avatar\/2788811\/avatar.png",
    "issues": 12,
    "commits": 11,
    "unit-tests": 0
  },
  "total-commits": 120,
  "total-issues": 56,
  "total-unit-tests": 0
}

describe('About', () => {

  beforeEach(() => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://tmvhaacw7f.execute-api.us-west-2.amazonaws.com/Prod/stats')
        .reply(200, TEST_RESPONSE);
  });

  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(<About/>);
    expect(wrapper).toMatchSnapshot();
  });
});
