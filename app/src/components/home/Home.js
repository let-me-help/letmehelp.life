import React, { Component } from 'react';
import Carousel from '../general/Carousel';
import CarouselSlide from '../general/CarouselSlide';
import Row from '../general/Row';
import Footer from '../main/Footer';
import carousel1 from '../../carousel1.png';
import carousel2 from '../../carousel2.jpg';
import carousel3 from '../../carousel3.jpg';
import CitiesByEventCount from '../visualizations/CitiesByEventCount';
import CitiesPopAndPoverty from '../visualizations/CitiesPopAndPoverty';
import StateInformationMap from '../visualizations/StateInformationMap';

import FoodCatByCity from '../visualizations/DeveloperVisualizations/FoodCatByCity';
import CharityCountByCity from '../visualizations/DeveloperVisualizations/CharityCountByCity';
import AvgAnnualIncomeCities from '../visualizations/DeveloperVisualizations/AvgAnnualIncomeCities';

import { EventCountCities, CityPopPoverty, USAMapStats,
    devFoodCatByCity, devCharityCountCity, devAvgIncomeCity } from '../../tools/constants';

class Home extends Component {
  render() {
    return (
      <div>
        <header>
          <Carousel name="HomePageCarousel">
            <CarouselSlide active img={carousel1} caption="Helping Hands Reach New Heights"/>
            <CarouselSlide img={carousel2} caption="People Helping Other People"/>
            <CarouselSlide img={carousel3} caption="Helping Tourists Learn About Other Cultures"/>
          </Carousel>
     
        </header>
        <hr/>
        <div className="container">
          <h2 className="text-center" style={{color: "#f06292"}}>
            Visualizations</h2>
          <br/>
          <br/>
          <Row columnType="col-md-10 mx-auto align-self-center">
            <StateInformationMap
              width={960} height={600}
              data={USAMapStats}/>
          </Row>
          <br/>
          <br/>
          <br/>
          <Row columnType="col-md-6 mx-auto align-self-center">
            <CitiesByEventCount 
              width={540} height={540}
              radius={180} data={EventCountCities}/>
            <p>
              This donut chart displays two pieces of information: number of events in ten 
              cities based on the event count, and the sum of all the events in 
              the ten cities. For instance, in the red slice, 82 represents the 
              number of events in the city of New York and the value 816 in the 
              center is the sum of all events in the ten cities.
            </p>
          </Row>   
          <Row columnType="col-md-6 mx-auto align-self-center">
            <p>
            You can see the population in top 10 US cities (based on event count) and 
            the number of people live below poverty line income in these cities.
            There are about 8.5 millions people live in New York city and 17% 
            out of those people has income under poverty line income. There are 
            roughly 26 Million people live in these top 10 cities and 4.85 Million 
            people live below poverty line income. (All the Data is from year 2016).
            </p>
            <CitiesPopAndPoverty 
              width={600} height={540}
              data={CityPopPoverty}/>
          </Row>
          <br/>
          <br/>
          <h2 className="text-center" style={{color: "#f06292"}}>
            Visualizations For Our Friends at
            <a href="http://charitylink2.me/"> Charity Link</a></h2>
          <br/>
          <br/>
          <Row columnType="col-md-10 mx-auto align-self-center">
            <FoodCatByCity
              width={960} height={500}
              data={devFoodCatByCity}/>
          </Row>
          <br/>
          <br/>
          <Row columnType="col-md-10 mx-auto align-self-center">
            <CharityCountByCity
              width={960} height={600}
              radius={180} data={devCharityCountCity}/>
          </Row>
          <h5 className="text-center" style={{color: "#f06292"}}>
          Average Annual Income in Ten Cities</h5>
          <br/>
          <Row columnType="col-md-10 mx-auto align-self-center">
            <AvgAnnualIncomeCities
              width={960} height={600}
              data={devAvgIncomeCity}/>
          </Row>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
