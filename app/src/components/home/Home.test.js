import React from 'react';
import { shallow } from 'enzyme';
import Home from './Home';
import Card from '../general/Card';
import Row from '../general/Row';
import Carousel from '../general/Carousel';
import CarouselSlide from '../general/CarouselSlide';

describe('Home Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<Home/>);

        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains a carousel with right amount of carousel slides', () => {
        const wrapper = shallow(<Home/>);

        expect(wrapper.find(Carousel).length).toEqual(1);
        expect(wrapper.find(CarouselSlide).length).toEqual(3);
    });

    // @Author Jorge
    it('contains correct amount of rows', () => {
        const wrapper = shallow(<Home/>);
        expect(wrapper.find(Row).length).toEqual(6);
    });
});