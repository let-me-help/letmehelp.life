import React, { Component } from 'react';
import '../../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../bootstrap/css/modern-business.css'
import '../../bootstrap/css/letmehelp.css'
import NavBar from './NavBar';
import Main from './Main';

class LetMeHelpApp extends Component {

  render() {
    return (
      <div>
        <NavBar/>
        <Main/>
      </div>
    );
  }
}

export default LetMeHelpApp;
