import React, { Component } from 'react';
import logo from '../../logo.png';
import { Link } from 'react-router-dom';

class NavBar extends Component {
  constructor(props){
    super(props);
    this.navButton = <div></div>;
    this.state = {
      searchTerm: ""
    };
  }

  handleSearchTerm(e){
    this.setState({'searchTerm': e.target.value})
  }

  forceHideNav() {
    this.navButton.click();
  }

  render() {
    return (
      <nav className="navbar fixed-top navbar-expand-lg navbar-light fixed-top">
      <div className="container">
        <Link className="navbar-brand" to="/">
          <img src={logo} width="50" height="50" alt="logo"></img>&nbsp;LetMeHelp</Link>
        <button
          ref={(navButton) => {this.navButton = navButton}}
          className="navbar-toggler navbar-toggler-right" type="button"
          data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ml-auto">
            <li
              className="nav-item"
              onClick={this.forceHideNav.bind(this)}>
              <Link className="nav-link" to="/charities/">Charities</Link>
            </li>
            <li
              className="nav-item"
              onClick={this.forceHideNav.bind(this)}>
              <Link className="nav-link" to="/events/">Events</Link>
            </li>
            <li
              className="nav-item"
              onClick={this.forceHideNav.bind(this)}>
              <Link className="nav-link" to="/cities/">Cities</Link>
            </li>
            <li
              className="nav-item"
              onClick={this.forceHideNav.bind(this)}>
              <Link className="nav-link" to="/about/">About</Link>
            </li>
            <li className="nav-item" style={{"paddingLeft": "20px"}}>
                <div className="form-inline">
                <input className="form-control mr-sm-2" style={{"width": "70%"}}
                  type="search" placeholder="Search" aria-label="Search"
                  onChange={(e) => this.handleSearchTerm(e)}/>
                <Link
                  to={`/search/${encodeURIComponent(this.state.searchTerm)}`}
                  className="btn btn-secondary"
                  onClick={this.forceHideNav.bind(this)}>Go
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    );
  }
}

export default NavBar;
