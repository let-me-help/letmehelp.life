import React from 'react';
import { shallow } from 'enzyme';
import NotFound404 from './404';


describe('404 Page Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<NotFound404/>);

        expect(wrapper).toMatchSnapshot();
    });
});