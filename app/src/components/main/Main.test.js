import React from 'react';
import { shallow } from 'enzyme';
import { Switch, Route } from 'react-router-dom'
import Main from './Main';

describe('Main component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<Main />);

        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains a switch and routes', () => {
        const wrapper = shallow(<Main />);

        expect(wrapper.find(Switch)).toBeTruthy();
        expect(wrapper.find(Route)).toBeTruthy();
    });
});