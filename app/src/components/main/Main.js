import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import Home from '../home/Home';
import About from '../about/About';
import CitiesModel from '../models/cities/CitiesModel';
import CharitiesModel from '../models/charities/CharitiesModel';
import EventsModel from '../models/events/EventsModel';
import CharityInstance from '../models/charities/CharityInstance';
import EventsInstance from '../models/events/EventsInstance';
import CityInstance from '../models/cities/CityInstance';
import SiteSearch from '../sitewidesearch/SiteSearch';
import NotFound404 from './404.js';

class Main extends Component {
	render() {
		return (
 			 <main>
  			<Switch>
  				<Route exact path='/' component={Home}/>
  				<Route exact path='/about' component={About}/>
  				<Route exact path='/cities' component={CitiesModel}/>
          <Route exact path='/charities' component={CharitiesModel}/>
  				<Route exact path='/events' component={EventsModel}/>
          <Route exact path='/search/:q' component={SiteSearch}/>
  				<Route exact path='/events/:id' component={EventsInstance}/>
  				<Route exact path='/charities/:id' component={CharityInstance}/>
  				<Route exact path='/cities/:id' component={CityInstance}/>
      		<Route path="/*" component={NotFound404}/>
  			</Switch>
  		</main>);
	}
}
export default Main;