import React, { Component } from 'react';
import logo from '../../logo.png';
import Row from '../general/Row';

class NotFound404 extends Component {
  render() {
    return (
      <div className="container">
        <br/><br/><br/><br/>
        <Row columnType="col-lg-12">
              <div>
              <img className="mx-auto d-block" src={logo} alt="" width="400" height="400"></img>,
              <div>
                <br/><br/>
                <h2 className="mx-auto text-center">Sorry, we can't help you with that <span role="img" aria-label="crying face">😢</span></h2>
                <p className="text-center">The page you requested does not exist (404).</p>
              </div>
              </div>
        </Row>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    );
  }
}

export default NotFound404;
