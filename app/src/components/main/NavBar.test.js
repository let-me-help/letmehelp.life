import React from 'react';
import { shallow } from 'enzyme';
import NavBar from './NavBar';
import { Link } from 'react-router-dom';


describe('Navigation Bar component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<NavBar/>);

        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains right number of links', () => {
        const wrapper = shallow(<NavBar />);

        expect(wrapper.find(Link).length).toBeGreaterThanOrEqual(5);
    });

});