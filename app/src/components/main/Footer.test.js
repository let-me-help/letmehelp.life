import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';

describe('Footer Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<Footer />);

        expect(wrapper).toMatchSnapshot();
    });
});