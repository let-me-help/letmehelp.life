import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="py-5">
        <div className="container">
          <p className="m-0 text-center text-white">Copyright &copy; LetMeHelp 2018</p>
        </div>
      </footer>
    );
  }
}

export default Footer;
