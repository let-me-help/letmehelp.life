import {Event, City, Charity} from './Templates';

describe('Event React model', () => {
    it('should contain the correct id', () => {
        const instance = Event(2);
        
        expect(instance.eventId).toEqual(2);
    });
});

describe('City React model', () => {
    it('should contain the correct id', () => {
        const instance = City('Austin');

        expect(instance.city_name).toEqual('Austin');
    });
});

describe('Charity React model', () => {
    it('should contain the correct id', () => {
        const instance = Charity(2);

        expect(instance.organizationId).toEqual(2);
    });
});