import React from 'react';
import jest from 'jest';
import { shallow } from 'enzyme';
import Model from './Model';

const TEST_MODEL =  (
    <Model
        name="Test"
        coverImage=""
        descriptionTitle="Test Model"
        description="The Best Model"
        exploreItems={["Cats", "Money", "Herbs"]}
        apiEndpoint="test"
        paginationFn={() => true}
    />);

describe('Model', () => {

  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(TEST_MODEL);
    expect(wrapper).toMatchSnapshot();
  });

});
