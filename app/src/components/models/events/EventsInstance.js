import ModelInstance from '../ModelInstance';
import { Link, Redirect } from 'react-router-dom';
import Card from '../../general/Card';
import GoogleMap from '../../general/GoogleMap';
import React, { Component } from 'react';
import axios from 'axios';
import { css } from 'react-emotion';
import { BounceLoader } from 'react-spinners';
import { BASE_API_URL, STYLES } from '../../../tools/constants';
import {Event, Charity, City} from '../Templates';
 
class EventInstance extends Component {
    constructor(props){
        super(props)

        this.id = this.props.match.params.id
        this.endpoint = `${BASE_API_URL}events/${this.id}`;
        this.state = {
            event: Event(this.id), 
            charity: Charity(""), 
            city: City(""),
            serverError: false,
            loading: true
        };
    }

    componentWillMount() {
        axios.get(this.endpoint)
            .then((eventResponse) => {
                const charityId = eventResponse.data.organizationId;
                const cityId = eventResponse.data.city;
                this.setState({
                    event: eventResponse.data, 
                    charity: Charity(charityId),
                    city: City(cityId)
                });
                return axios.get(`${BASE_API_URL}charities/${charityId}`);
            })
            .then((charityResponse) => {
                const cityId = this.state.event.city;
                this.setState({
                    event: this.state.event, 
                    charity: charityResponse.data, 
                    city: this.state.city
                });
                return axios.get(`${BASE_API_URL}cities/${cityId}`);
            })
            .then((cityResponse) => {
                this.setState({
                    event: this.state.event, 
                    charity: this.state.charity, 
                    city: cityResponse.data,
                    loading: false
                });
            })
            .catch((error) =>
                this.setState({serverError: true})
            );
    }

    render() {
        if (this.state.serverError){
            return <Redirect to="/404" />
        } else if (this.state.loading) {
            const override = css`
            display: block;
            margin: 0 auto;
            `;
            return ( 
                <div className="container" style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100vh"}}>
                    <BounceLoader
                        className={override}
                        sizeUnit={"px"}
                        size={250}
                        color={'#f8bbd0'}
                        loading={this.state.loading}
                    />
                </div>
            );
        } else {
            return (
            <ModelInstance  type="Events" typeLink="/events"
                body={this.body()} cover={this.cover()}
                footer={this.footer()}
                title={
                    <p>
                        {this.state.event.name}
                        <br/>
                        <small>
                            by {" "}
                            <Link to={`/charities/${this.state.charity.organizationId}`}>
                                {this.state.charity.name} 
                            </Link>
                        </small>
                    </p>
                }
                name={this.state.event.name}
                cards={this.cards()}/>
            );
        }
    }

    cards() {
        const timeFormat =  {hour: '2-digit', minute:'2-digit'}
        return [
            <Card key="1" header={<h5>Date and Time</h5>} body={
                <p>
                    { new Date(this.state.event.startTime).toDateString() }
                    <br/>
                    { new Date(this.state.event.startTime).toLocaleTimeString('en-US', timeFormat)}
                    {" "}
                    – 
                    {" "}
                    { new Date(this.state.event.endTime).toLocaleTimeString('en-US', timeFormat)}
                </p>
            }/>,
            <Card key="2" header={<h5>Location</h5>} body={
                <p>
                    { this.state.event.address_1 }
                    <br/>
                    { this.state.event.address_2 }
                    <br/>
                    <Link to={`/cities/${this.state.city.city_name}`}>{this.state.city.city_name}</Link>,{" "}
                    {this.state.city.state} {" "}
                    { this.state.event.zipcode}
                </p>
            }/>,
        ]
    }

    body() {
        return (
            <div>
                <p>
                    {this.state.event.descriptionText}
                </p>
            </div>
        ); 
    }

    cover() {
        return (
            <img width="750px" style={STYLES.FIT_IMG_STYLE} className="img-fluid" alt="Event Banner" 
                src={this.state.event.logoUrl}/>
        ); 
    }

    footer() {
        const locationQuery = this.state.event.address_1
            .split(" ").concat([this.state.event.address_2, 
                    this.state.city.city_name, this.state.city.state])
            .join("+");
        return (<GoogleMap qry={locationQuery}/>);
    }
}


export default EventInstance