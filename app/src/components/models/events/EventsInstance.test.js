import React from 'react';
import { shallow } from 'enzyme';
import EventsInstance from './EventsInstance';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {Event, City, Charity} from '../Templates';
import { BASE_API_URL } from '../../../tools/constants';
import GoogleMap from '../../general/GoogleMap';

const EVENT_TEST_RESPONSE = Event(1);
const CITY_TEST_RESPONSE = City("Test")
const CHARITY_TEST_RESPONSE = Charity(2);

describe('Instance of Event Component', () => {
    beforeEach(() => {
        EVENT_TEST_RESPONSE.organizationId = 2;
        EVENT_TEST_RESPONSE.city = "Test";
        const mock = new MockAdapter(axios);
        mock.onGet(`${BASE_API_URL}events/1`)
            .reply(200, EVENT_TEST_RESPONSE);
        mock.onGet(`${BASE_API_URL}charities/2`)
            .reply(200, CHARITY_TEST_RESPONSE);
        mock.onGet(`${BASE_API_URL}cities/Test`)
            .reply(200, CITY_TEST_RESPONSE);
    });

    // @Author Jorge
    it ('renders correctly', () => {
        const wrapper = shallow(<EventsInstance match={ {params: {id: 1} } }/>)
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('has the correct number of cards', () => {
        const wrapper = shallow(<EventsInstance match={ {params: {id: 1} } }/>)
        const instance = wrapper.instance();
        expect(instance.cards().length).toBeGreaterThanOrEqual(2);
    });

    // @Author Jorge
    it('contains Google Map component', () => {
        const wrapper = shallow(<EventsInstance match={ {params: {id: 1} } }/>)
        const instance = wrapper.instance();
        expect(shallow(instance.footer()).find(GoogleMap)).toBeTruthy();
    });

    // @Author Jorge
    it('contains cover image', () => {
        const wrapper = shallow(<EventsInstance match={ {params: {id: 1} } }/>)
        const instance = wrapper.instance();
        expect(shallow(instance.cover()).find('img')).toBeTruthy();
    });
});