import React from 'react';
import { shallow } from 'enzyme';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import EventsModel, { createEventCardFn } from './EventsModel';

const TEST_RESPONSE = {
	address_1: "25829 Cinco Ranch Boulevard",
	address_2: "",
	address_city: "Katy",
	city: "Houston",
	descriptionHTML: "Join us for a 5K",
	descriptionText: "Join us for a 5K",
	endTime: "2018-11-11T12:30:00",
	eventId: "49322331421",
	eventUrl: "https://www.eventbrite.com/thing",
	latitude: "29.7259425",
	localized_address_display: "somewhere",
	logoUrl: "https://something.com",
	longitude: "-95.79998130000001",
	name: "Unity Run - 5K",
	organizationId: "17439237077",
	startTime: "2018-11-11T09:30:00",
	state: "TX",
	venueId: "26316648",
	zipcode: "77494"
}

describe('EventsModel', () => {

  beforeEach(() => {
    const mock = new MockAdapter(axios);
    mock.onGet('https://devapi.letmehelp.life/charities')
        .reply(200, []);
  });

  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(<EventsModel/>);
    expect(wrapper).toMatchSnapshot();
  });

  // @author Daniel
  it('creates card correctly', () => {
    expect(createEventCardFn()(TEST_RESPONSE)).toBeTruthy();
  });
});
