import coverImage from './event.png';
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Model from '../Model';
import Card from '../../general/Card';
import OrganizerSettings from '../../pagination/OrganizerSettings';
import { SelectorTypes } from '../../pagination/Organizer';
import { State } from '../../pagination/Results';
import { getExpandableStr, getHighlightedDiv } from '../../../tools/StringTools';

const NAME = "Events";

function createOrganizerSettings(charities) {
  if (!charities) {
    charities = [];
  }
  const organizerSettings = new OrganizerSettings(NAME);
  organizerSettings.addFilter(SelectorTypes.TYPEAHEAD, 'Charity', charities, 'charity_id');
  organizerSettings.addFilter(SelectorTypes.TYPEAHEAD, 'State', State, 'state');
  organizerSettings.addFilter(SelectorTypes.DATEPICKER, 'Date', [], 'date');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Date', ['newer', 'older', 'None'], 'date');
  return organizerSettings;
}


export function createEventCardFn(keyWord) {
  return (response) => {
    const timeFormat =  {hour: '2-digit', minute:'2-digit'};
    const dateStr = new Date(response.startTime).toDateString();
    const startTime = new Date(response.startTime).toLocaleTimeString('en-US', timeFormat);
    const endTime = new Date(response.endTime).toLocaleTimeString('en-US', timeFormat);
    const timeStr = `${startTime} - ${endTime}`;
    const eventLink = `/events/${response.eventId}`;
    const locationLink = `/cities/${response.city}`;
    const eventName = getExpandableStr(response.name, keyWord);
    const header = <Link to={eventLink}>{eventName.short}</Link>;
    const hoverHeader = <Link to={eventLink}>{eventName.expanded}</Link>;
    const addressCity = getHighlightedDiv(response.address_city, keyWord);
    const address1 = getHighlightedDiv(response.address_1, keyWord);
    return (<Card
              hover
              hoverShadow
              key="event-card"
              header={header}
              hoverHeader={hoverHeader}
              body={[
                <p>
                  <b>{dateStr}</b><br/>
                     {timeStr}<br/>
                </p>,
                <Link to={locationLink}>
                  <div>{addressCity}</div>
                  <div>{address1}</div>
                </Link>
                ]
            }/>);
  };
}

class EventsModel extends Component {

  constructor(props) {
    super(props);
    this.state = {organizerSettings: createOrganizerSettings()};
  }

  componentDidMount() {
    axios.get("https://devapi.letmehelp.life/charities")
      .then((response) => {
        const charityNames = response.data.map((obj) => ({"id": obj.organizationId ,"label": obj.name}));
        this.setState({
          organizerSettings: createOrganizerSettings(charityNames)
        });
      });
  }

	render() {
		return (
 			 <Model
          name={NAME}
          coverImage={coverImage}
          descriptionTitle="Find a local Event"
          description="LetMeHelp has various event postings to help
                       you find a local charity event. We aim to provide
                       you with an easy way to get connected with local
                       community organizers and help out!"
          apiEndpoint="events"
          paginationFn={createEventCardFn}
          organizerSettings={this.state.organizerSettings}
        />
              );
	}
}
export default EventsModel;
