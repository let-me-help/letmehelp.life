import React, { Component } from 'react';
import BreadCrumb from '../general/BreadCrumb';
import Row from '../general/Row';

class ModelInstance extends Component {
    render() {
        return (
            <div className="container">
                <h1 className="mt-4 mb-3">{ this.props.title }</h1>
                <BreadCrumb crumbs={["Home", this.props.type, this.props.name]} 
                    links={["/", this.props.typeLink]}/>
                <Row columnTypes={["col-lg-8", "col-md-4"]}>
                    <div>
                        {this.props.cover}
                        <hr/>
                        {this.props.body}
                        <hr/>
                        {this.props.footer}
                    </div>
                    { this.props.cards }
                </Row>
            </div>
        );
    }
}

export default ModelInstance