/* 
    The purpose of these functions is just to generate
    Javascript object that are associated with the responses
    from the API endpoints we have for events, cities, and 
    charities.
*/

const Event = (id) => {
    return {
        "eventId": id,
        "logoUrl": "",
        "descriptionHTML": "",
        "address_city": "",
        "address_1": "",
        "name": "",
        "eventUrl": "",
        "address_2": "",
        "state": "",
        "city": "",
        "startTime": "",
        "zipcode": "",
        "localized_address_display": "",
        "endTime": "",
        "organizationId": "",
        "venueId": "",
        "longitude": "",
        "descriptionText": "",
        "latitude": ""
    }
}

const Charity = (id) => {
   return {
        "organizationId": id,
        "name": "",
        "cause": "",
        "charityWebsite": "",
        "city": "",
        "descriptionHTML": "",
        "descriptionText": "",
        "donationUrl": "",
        "latitude": "",
        "logoUrl": "",
        "longitude": "",
        "socialMedia": {
            "twitter": "",
            "facebook": ""
        },
        "address": "",
        "state": "",
        "zipCode": ""
    }
}

const City = (id) => {
    return {
        "city_name": id,
        "children_in_poverty": "",
        "data_year": "",
        "description": "",
        "income_below_poverty": "",
        "population": "",
        "state": "",
    }
}

export {Event, Charity, City}