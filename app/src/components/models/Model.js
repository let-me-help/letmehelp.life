import React, { Component } from 'react';
import OrganizedPagination from '../pagination/OrganizedPagination';
import BreadCrumb from '../general/BreadCrumb';
import Row from '../general/Row';

class Model extends Component {

  render() {
    return (
    <div className="container">
      <br/><br/>
      <BreadCrumb crumbs={["Home", this.props.name]}
                  links={["/"]} />
      <Row columnTypes={["col-md-4", "col-md-2", "col-md-6"]}>
          <div>
            <h3 className="my-3">{this.props.descriptionTitle}</h3>
            <p>{this.props.description}</p>
          </div>
          <div></div>
          <div>
          <img className="img-fluid" src={this.props.coverImage} alt=""></img>
          </div>
      </Row>
      <h3 className="my-4">Browse {this.props.name}</h3>
      <OrganizedPagination
            rowCount={3}
            colCount={4}
            apiEndpoint={this.props.apiEndpoint}
            createCardFn={this.props.paginationFn}
            organizerSettings={this.props.organizerSettings}
        />
    </div>
    );
  }
}

export default Model;
