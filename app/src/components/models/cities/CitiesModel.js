import coverImage from "./cities.jpg";
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Card from '../../general/Card';
import Model from '../Model';
import OrganizerSettings from '../../pagination/OrganizerSettings';
import { SelectorTypes } from '../../pagination/Organizer';
import { State } from '../../pagination/Results';
import { getExpandableStr } from '../../../tools/StringTools';

const NAME = "Cities";

function createOrganizerSettings() {
  const organizerSettings = new OrganizerSettings(NAME);
  organizerSettings.addFilter(SelectorTypes.TYPEAHEAD, 'State', State, 'state');
  organizerSettings.addFilter(SelectorTypes.RANGESLIDER, 'Population', {'min': 500000, 'max': 10000000, 'step': 10000}, 'population');
  organizerSettings.addFilter(SelectorTypes.RANGESLIDER, 'Child Poverty Rate', {'min': 0, 'max': 1, 'step': 0.025}, 'child_poverty_rate');
  organizerSettings.addFilter(SelectorTypes.RANGESLIDER, 'Population Below Poverty Line', {'min': 1000, 'max': 2000000, 'step': 1000}, 'income_below_poverty');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Name', ['A-Z', 'Z-A', 'None'], 'city_name');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Population', ['higher', 'lower', 'None'], 'population');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Child Poverty Rate', ['higher', 'lower', 'None'], 'child_poverty_rate');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Population Below Poverty Line', ['higher', 'lower', 'None'], 'income_below_poverty');
  return organizerSettings;
}


const COMMA_REGEX = /\B(?=(\d{3})+(?!\d))/g;

export function createCityCardFn(keyWord) {
  return (response) => {
    const population = response.population
        .toString().replace(COMMA_REGEX, ",");
    const povertyPopulation = parseInt(response.income_below_poverty, 10)
        .toString().replace(COMMA_REGEX, ",");
    const childrenPoverty = Math.round((response.children_in_poverty * 100) * 10 ) / 10;
    const dataYear = response.data_year;
    const locationLink = `/cities/${response.city_name}`;
    let locationStr = `${response.city_name},\xa0${response.state}`;
    locationStr = getExpandableStr(locationStr, keyWord);
    const header = <Link to={locationLink}>{locationStr.short}</Link>;
    const hoverHeader = <Link to={locationLink}>{locationStr.expanded}</Link>;

    return (<Card
              hover
              hoverShadow
              header={header}
              hoverHeader={hoverHeader}
              body={[
                <p>
                  <b>Population: </b> {population}
                </p>,
                <p>
                  <b>Population Living Below Poverty Line:</b> {povertyPopulation}
                </p>,
                <p>
                  <b>Children in Poverty: </b>{childrenPoverty}%
                </p>,
                <p>
                  <b>Data Year: </b>{dataYear}
                </p>
            ]}/>);
  };
}

class CitiesModel extends Component {
	render() {
		return (
 			 <Model name={NAME}
              coverImage={coverImage}
              descriptionTitle="Help your City"
              description="LetMeHelp provides the convenience of finding
                           charities and charity events by locations. You can
                           explore locations using several search criteria such as
                           distance, state, economy and more."
              apiEndpoint="cities"
              paginationFn={createCityCardFn}
              organizerSettings={createOrganizerSettings()}
         />);
	}
}
export default CitiesModel;
