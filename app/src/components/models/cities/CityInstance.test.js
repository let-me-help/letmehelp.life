import React from 'react';
import { shallow } from 'enzyme';
import CityInstance from './CityInstance';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {Event, City, Charity} from '../Templates';
import { BASE_API_URL } from '../../../tools/constants';
import GoogleMap from '../../general/GoogleMap';

const EVENT_TEST_RESPONSE = [Event(2), Event(3), Event(1)];
const CITY_TEST_RESPONSE = City("New York");
const CHARITY_TEST_RESPONSE = [Charity(2), Charity(3), Charity(1)];

describe('Instance of Event Component', () => {
    beforeEach(() => {
        EVENT_TEST_RESPONSE.map((event) => {
            event.organizationId = event.eventId;
            event.city = "Test";
        });
        
        CHARITY_TEST_RESPONSE.map((charity) => {
            charity.city = CITY_TEST_RESPONSE.city_name;
        });

        const mock = new MockAdapter(axios);
        mock.onGet(`${BASE_API_URL}cities/New York`)
            .reply(200, CITY_TEST_RESPONSE);
        mock.onGet(`${BASE_API_URL}events?city=New York&max_results=3`)
            .reply(200, EVENT_TEST_RESPONSE);
        mock.onGet(`${BASE_API_URL}charities?city=New York&max_results=3`)
            .reply(200, CHARITY_TEST_RESPONSE);
       
    });

    // @Author Jorge
    it ('renders correctly', () => {
        const wrapper = shallow(<CityInstance match={ {params: {id: "New York"} } }/>)
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('has the correct number of cards', () => {
        const wrapper = shallow(<CityInstance match={ {params: {id: "New York"} } }/>)
        const instance = wrapper.instance();
        expect(instance.cards().length).toBeGreaterThanOrEqual(3);
    });

    // @Author Jorge
    it('contains Google Map component', () => {
        const wrapper = shallow(<CityInstance match={ {params: {id: "New York"} } }/>)
        const instance = wrapper.instance();
        expect(shallow(instance.footer()).find(GoogleMap)).toBeTruthy();
    });
});