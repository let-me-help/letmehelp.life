import React from 'react';
import { shallow } from 'enzyme';
import CitiesModel, { createCityCardFn } from './CitiesModel';

const TEST_RESPONSE = {
	children_in_poverty: "0.21",
	city_name: "New York",
	data_year: "2016",
	description: "test",
	income_below_poverty: "1.0",
	population: "8537673",
	state: "New York"
};

describe('CitiesModel', () => {
  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(<CitiesModel/>);
    expect(wrapper).toMatchSnapshot();
  });

  // @author Daniel
  it('creates card correctly', () => {
    expect(createCityCardFn()(TEST_RESPONSE)).toBeTruthy();
  });
});
