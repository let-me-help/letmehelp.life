import ModelInstance from '../ModelInstance';
import { Link, Redirect } from 'react-router-dom';
import Card from '../../general/Card';
import GoogleMap from '../../general/GoogleMap';
import React, { Component } from 'react';
import axios from 'axios';
import { css } from 'react-emotion';
import { BounceLoader } from 'react-spinners';
import { BASE_API_URL } from '../../../tools/constants';
import { City } from '../Templates';


class CityInstance extends Component {
    constructor(props){
        super(props)

        this.id = this.props.match.params.id
        this.endpoint = `${BASE_API_URL}cities/${this.id}`
        this.state = {
            charities: [], 
            city: City(this.id), 
            events: [],
            serverError: false, 
            loading: true
        };
    }

    componentWillMount() {
        axios.get(this.endpoint)
            .then((cityResponse) => {
                this.setState({
                    city: cityResponse.data
                });
                return axios.get(`${BASE_API_URL}charities?city=${this.id}&max_results=3`);
            })
            .then((charityResponse) => {
                this.setState({
                    charities: charityResponse.data
                });
                return axios.get(`${BASE_API_URL}events?city=${this.id}&max_results=3`);
            })
            .then((eventsResponse) => {
                this.setState({
                    events: eventsResponse.data,
                    loading: false
                });
            })
            .catch((error) => 
                this.setState({serverError: true})
            );
    }

    render() {
        if (this.state.serverError){
            return <Redirect to="/404" />
        } else if (this.state.loading) {
            const override = css`
            display: block;
            margin: 0 auto;
            `;
            return ( 
                <div className="container" style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100vh"}}>
                    <BounceLoader
                        className={override}
                        sizeUnit={"px"}
                        size={250}
                        color={'#f8bbd0'}
                        loading={this.state.loading}
                    />
                </div>
            );
        } else {
        return <ModelInstance  type="Cities" typeLink="/cities"
                    body={this.body()} cover={this.cover()}
                    footer={this.footer()}
                    title={
                        <p>
                            {this.state.city.city_name}, {" "}
                            {this.state.city.state}
                        </p>
                    }
                    name={this.state.city.city_name}
                    cards={this.cards()}/>
        }
    }

    cover() {
        let locQry = this.state.city.city_name
        if (locQry === "New York"){
            locQry = locQry + " City"
        }
        locQry = locQry.split(" ")
                    .concat(this.state.city.state.split(" "))
                    .join("+");
        return <GoogleMap qry={locQry} />
    }

    body() {
        return <p>{this.state.city.description}</p>
    }

    footer() {
        return <div></div>
    }

    cards() {
        return [
            <Card key="1" header={<h5>Charities</h5>} body={
                this.state.charities.map((charity) => {
                    return (
                        <div>
                            <Link to={`/charities/${charity.organizationId}`}> 
                                {charity.name} 
                            </Link>
                            <br/>
                        </div>
                    );
                })
            }/>,
            <Card key="2" header={<h5>Events</h5>} body={
                this.state.events.map((event, k) => {
                    return (
                        <div key={k}>
                            <Link to={`/events/${event.eventId}`}> 
                                {event.name} 
                            </Link>
                            <br/>
                        </div>
                    );
                })
            }/>,
            <Card key="3" header={<h5>Stats</h5>} body={
                <div>
                    <p>
                        <strong>Population</strong>: {" "}
                        {
                            parseInt(this.state.city.population)
                                .toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        }
                    </p>
                    <p>
                        <strong>Population Living Below Poverty Line</strong>: {" "}
                        {
                            parseInt(this.state.city.income_below_poverty)
                                .toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        }
                    </p>
                    <p>
                        <strong>Children in Poverty</strong>: {" "}
                        {
                            (parseFloat(this.state.city.children_in_poverty) * 100)
                                .toFixed(2).toString() + " %"
                        }
                    </p>
                </div>
            }/>,
        ]
    }
}

export default CityInstance;