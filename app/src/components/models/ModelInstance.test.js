import React from 'react';
import { shallow } from 'enzyme';
import ModeInstance from './ModelInstance';
import Card from '../general/Card';

describe('Model Instance component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(
            <ModeInstance type="Test" typeLink="/Test"
            body={<p>This is a body</p>} cover={<img src=""/>}
            footer={<p>This is a footer</p>}
            title={<p> This is only a test </p>}
            name={<p>Test</p>}
            cards={[<Card key="1" header={<p>Test Card</p>} body={<p>Test Card</p>}/>]}/>
        );

        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains cards', () => {
        const wrapper = shallow(
            <ModeInstance type="Test" typeLink="/Test"
            body={<p>This is a body</p>} cover={<img src=""/>}
            footer={<p>This is a footer</p>}
            title={<p> This is only a test </p>}
            name={<p>Test</p>}
            cards={[<Card key="1" header={<p>Test Card</p>} body={<p>Test Card</p>}/>]}/>
        );

        expect(wrapper.find(Card)).toBeTruthy();
    });
});