import coverImage from './charity.jpg';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Model from '../Model';
import Card from '../../general/Card';
import OrganizerSettings from '../../pagination/OrganizerSettings';
import { SelectorTypes } from '../../pagination/Organizer';
import { Causes, Cities } from '../../pagination/Results';
import { getExpandableStr, getHighlightedDiv } from '../../../tools/StringTools';

const NAME = "Charities";

function createOrganizerSettings() {
  const organizerSettings = new OrganizerSettings(NAME);
  organizerSettings.addFilter(SelectorTypes.TYPEAHEAD, 'City', Cities, 'city');
  organizerSettings.addFilter(SelectorTypes.TYPEAHEAD, 'Cause', Causes, 'cause');
  organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Charity Name', ['A-Z', 'Z-A', 'None'], 'name');
  return organizerSettings;
}

export function createCharityCardFn(keyWord) {
  return function(response) {
    let location = response.city + ",\xa0" + response.state;
    if (keyWord) {
      location = getHighlightedDiv(location, keyWord);
    }
    const charityLink = `/charities/${response.organizationId}`;
    const locationLink = `/cities/${response.city}`;
    const charityName = getExpandableStr(response.name, keyWord);
    const header = <Link to={charityLink}>{charityName.short}</Link>;
    const hoverHeader = <Link to={charityLink}>{charityName.expanded}</Link>;
    const cause = getExpandableStr(response.cause, keyWord, 15);
    const bottomLinks = (
      <span>
        <a href={response.donationUrl}><b>DONATE</b></a>{" or "}
        <a href={response.charityWebsite}><b>Visit Site</b></a>
      </span>);
    const body = [
      <p><strong>{cause.short}</strong></p>,
      <Link to={locationLink}>{location}</Link>,
      <br/>,
      bottomLinks];
    const hoverBody = [
      <p><strong>{cause.expanded}</strong></p>,
      <Link to={locationLink}>{location}</Link>,
      <br/>,
      bottomLinks];

    return (<Card
              hover
              hoverShadow
              header={header}
              hoverHeader={hoverHeader}
              body={body}
              hoverBody={hoverBody}
            />);
  };
}

class CharitiesModel extends Component {
	render() {
		return (
 			 <Model name={NAME}
              coverImage={coverImage}
              descriptionTitle="Find Your Charity"
              description="LetMeHelp has an assortment of different charities from
                           across the country. Whether you are looking to contribute
                           to your local community or finding out if you can lend a hand
                           from miles away, LetMeHelp can assist your charity searching needs."
              apiEndpoint="charities"
              paginationFn={createCharityCardFn}
              organizerSettings={createOrganizerSettings()}
         />);
	}
}
export default CharitiesModel;
