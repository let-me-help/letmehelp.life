import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import ModelInstance from '../ModelInstance';
import Card from '../../general/Card';
import GoogleMap from '../../general/GoogleMap';
import SocialMediaFeed from '../../general/SocialMedia/SocialMediaFeed';
import axios from 'axios';
import { css } from 'react-emotion';
import { BounceLoader } from 'react-spinners';
import { BASE_API_URL, STYLES } from '../../../tools/constants';
import { Charity, City } from '../Templates';


class CharityInstance extends Component {
    constructor(props){
        super(props)

        this.id = this.props.match.params.id
        this.endpoint = `${BASE_API_URL}charities/${this.id}`
        this.state = {
            charity: Charity(this.id), 
            city: City(""), 
            events: [],
            serverError: false,
            loading: true
        };
    }


    componentWillMount() {
        axios.get(this.endpoint)
            .then((charityResponse) => {
                const cityId = charityResponse.data.city;
                this.setState({
                    charity: charityResponse.data
                });
                return axios.get(`${BASE_API_URL}cities/${cityId}`);
            })
            .then((cityResponse) => {
                this.setState({
                    city: cityResponse.data
                });
                return axios.get(`${BASE_API_URL}events?charity_id=${this.id}&max_results=3`);
            })
            .then((eventsResponse) => {
                this.setState({
                    events: eventsResponse.data,
                    loading: false
                });
            })
            .catch((error) =>
                this.setState({serverError: true})
            );
    }

    render() {
        if (this.state.serverError){
            return <Redirect to="/404" />
        } else if (this.state.loading) {
            const override = css`
                display: block;
                margin: 0 auto;
                `;
           return ( 
               <div className="container" style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100vh"}}>
                    <BounceLoader
                        className={override}
                        sizeUnit={"px"}
                        size={250}
                        color={'#f8bbd0'}
                        loading={this.state.loading}
                    />
                </div>
            );
        } else {
            return <ModelInstance  type="Charities" typeLink="/charities"
                        body={this.body()} cover={this.cover()}
                        footer={this.footer()}
                        title={
                            <p>
                                {this.state.charity.name}
                            </p>
                        }
                        name={this.state.charity.name}
                        cards={this.cards()}/>
        }
    }

    cover() {
        return (
            <img width="750px" style={STYLES.FIT_IMG_STYLE} className="img-fluid" alt="Charity Banner" 
                src={this.state.charity.logoUrl}/>
        ); 
      
    }

    body() {
        const causeBadge = (
            <span className="badge badge-pill badge-info">
                {this.state.charity.cause}
            </span>
        );
        return (
            <div>
                <p>
                    {this.state.charity.descriptionText}
                </p>
                {causeBadge}
            </div>
        ); 
    }

    footer() {
        const locQry = [].concat([this.state.charity.latitude, 
                            this.state.charity.longitude])
                         .join("+");
        return (
            <figure className="figure" style={ {"width": "100%"} }>
                <GoogleMap qry={locQry} disableURI/>
                <figcaption className="figure-caption">
                    Please note that the above location could refer to a 
                    specific chapter of the larger organization
                </figcaption>
            </figure> );
    }

    cards() {
        const website = this.state.charity.charityWebsite;
        return [
            <Card key="1" header={<h5>Contact</h5>} body={
                <p>
                    <a href={website}>Website</a>
                    <br/>
                    <Link to={`/cities/${this.state.city.city_name}`}>
                        { this.state.city.city_name }
                    </Link>, 
                    {" "} {this.state.city.state} {" "}
                    { this.state.charity.zipCode }
                </p>
            }/>,
            <Card key="2" header={<h5>Events</h5>} body={
                (this.state.events.length === 0) ? 
                <p>No Events Listed. Please check back later!</p> :
                this.state.events.map((event, k) => {
                    return (
                        <div key={k}>
                            <Link to={`/events/${event.eventId}`}> {event.name} </Link>
                            <br/>
                        </div>
                    );
                })
            }/>,
            <Card key="3" header={
                <a href={this.state.charity.donationUrl}>
                    <font size="20">Donate</font>
                </a>
            } body={<p>Please consider donating to this wonderful charity.</p>}/>,
            <SocialMediaFeed key="4" accounts={this.state.charity.socialMedia} />
        ]
    }
}


export default CharityInstance