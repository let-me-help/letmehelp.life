import React from 'react';
import { shallow } from 'enzyme';
import CharitiesModel, { createCharityCardFn } from './CharitiesModel';

const TEST_RESPONSE = {
	cause: "",
	charityWebsite: "http://www.google.com/something",
	city: "Phoenix",
	descriptionHTML: "",
	descriptionText: "",
	donationUrl: "",
	latitude: "",
	logoUrl: "",
	longitude: "",
	name: "Secular Coalition for Arizona",
	organizationId: "",
};

describe('CharitiesModel', () => {
  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(<CharitiesModel/>);
    expect(wrapper).toMatchSnapshot();
  });

  // @author Daniel
  it('creates card correctly', () => {
    expect(createCharityCardFn()(TEST_RESPONSE)).toBeTruthy();
    TEST_RESPONSE["charityWebsite"] = "something.com";
    expect(createCharityCardFn()(TEST_RESPONSE)).toBeTruthy();
  });
});
