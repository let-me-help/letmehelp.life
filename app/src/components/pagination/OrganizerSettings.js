/**
 * This object is meant to be using in conjuction with the
 * Organizer component. This class will be used by the
 * component to generate the appropiate user input fields.
 *
 * Usage Example:
 *
 * const organizerSettings = new OrganizerSettings("Test");
 * organizerSettings.addFilter(SelectorTypes.DATEPICKER, 'Date', [], 'selectedDate'); 
 * organizerSettings.addFilter(SelectorTypes.RANGESLIDER, 'Poverty Rate', {'min': 0, 'max': 20, 'step': 1}, 'povertyRateRange');
 * organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Name', ['A-Z', 'Z-A'], 'nameSort');
 * organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Stats', ['ascending', 'descending'], 'nameSort');
 * organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Charity Name', ['A-Z', 'Z-A'], 'nameSort');
 */
class OrganizerSettings {

    constructor(name) {
        this.name = name;
        this.sort = {};
        this.filter = {};
    }

    addFilter(type, label, values, id) {
        const newFilter = {
            "label": label,
            "vals": values,
            "id": id
        }
        if (type in this.filter) {
            this.filter[type].push(newFilter);
        } else {
            this.filter[type] = [newFilter];
        }
    }

    addSort(type, label, values, id) {
        const newSort = {
            "label": label,
            "vals": values,
            "id": id
        }
        if (type in this.sort) {
            this.sort[type].push(newSort);
        } else {
            this.sort[type] = [newSort];
        }
    }
}

export default OrganizerSettings;