import OrganizerSettings from './OrganizerSettings';
import { SelectorTypes } from './Organizer';

describe('OrganizerSettings', () => {
    //@Author Jorge
    it('contains the correct settings', () => {
        const organizerSettings = new OrganizerSettings("Test");

        organizerSettings.addFilter(SelectorTypes.DATEPICKER, 'Date', [], 'selectedDate');
        organizerSettings.addSort(SelectorTypes.RADIOBUTTON, 'Name', ['A-Z', 'Z-A'], 'nameSort');

        expect(organizerSettings.filter[SelectorTypes.DATEPICKER]).toBeTruthy();
        expect(organizerSettings.sort[SelectorTypes.RADIOBUTTON]).toBeTruthy();
    });
});