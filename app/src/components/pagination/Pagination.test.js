import React from 'react';
import jest from 'jest';
import { shallow } from 'enzyme';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Card from '../general/Card';
import Pagination from './Pagination';
import { BASE_API_URL } from '../../tools/constants';

function createTestResponse() {
    const items = [];
    for(let i = 0; i < 24; i++) {
        items.push({
            id: i,
            name: "#" + i,
            description: i + " description"
        });
    }
    return items;
}

function createTestCard(response) {
    return (
        <Card
            key="test"
            header={response.name}
            body={[response.id, response.description]}
        />);
}

const TEST_PAGINATION =  (
    <Pagination
        rowCount={3}
        colCount={4}
        apiEndpoint="test"
        cardFn={() => true}
    />);

describe('Pagination', () => {

  beforeEach(() => {
    const mock = new MockAdapter(axios);
    mock.onGet(`${BASE_API_URL}test?max_results=12&offset=0`)
        .reply(200, createTestResponse());
  });

  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(TEST_PAGINATION);
    expect(wrapper).toMatchSnapshot();
  });

  // @author Daniel
  it('creates full pages correctly', () => {
    const wrapper = shallow(TEST_PAGINATION);
    setTimeout(
        () => {
            const state = wrapper.state();
            // 24 items should have been saved
            const items = state.items;
            expect(items.length).toBe(24);
            // Check content of the items
            expect(items[0].id).toBe(0);
            expect(items[23].id).toBe(23);

            // 3 rows should have been created
            expect(state.rows.length).toBe(6);
            // each row should have 4 columns
            const firstRow = state.rows[0];
            expect(firstRow.props.children.length).toBe(4);
        },
        1000);
  });

  // @author Daniel
  it('creates partial pages correctly', () => {
    const mock = new MockAdapter(axios);
    mock.onGet(`${BASE_API_URL}test?max_results=12&offset=0`)
        .reply(200, createTestResponse().slice(1));
    const wrapper = shallow(TEST_PAGINATION);
    setTimeout(
        () => {
            const state = wrapper.state();
            // 24 items should have been saved
            const items = state.items;
            expect(items.length).toBe(23);
            // Check content of the items
            expect(items[0].id).toBe(1);
            expect(items[22].id).toBe(23);

            // 3 rows should have been created
            expect(state.rows.length).toBe(6);
            // each row should have 4 columns
            const firstRow = state.rows[0];
            expect(firstRow.props.children.length).toBe(4);
        },
        1000);
  });
});
