let CityState = {
    "Portland": "Oregon",
    "New York": "New York",
    "Seattle": "Washington",
    "Las Vegas": "Nevada",
    "El Paso": "Texas",
    "Baltimore": "Maryland",
    "Columbus": "Ohio",
    "Boston": "Massachusetts",
    "Chicago": "Illinois",
    "Nashville": "Tennessee",
    "Oklahoma City": "Oklahoma",
    "San Jose": "California",
    "Dallas": "Texas",
    "Los Angeles": "California",
    "Denver": "Colorado",
    "Fort Worth": "Texas",
    "Houston": "Texas",
    "Detroit": "Michigan",
    "Washington": "District of Columbia",
    "Jacksonville": "Florida",
    "Phoenix": "Arizona",
    "San Francisco": "California",
    "Philadelphia": "Pennsylvania",
    "Charlotte": "North Carolina",
    "San Antonio": "Texas",
    "TestCity": "TestState",
    "Indianapolis": "Indiana",
    "Louisville": "Kentucky",
    "San Diego": "California",
    "Memphis": "Tennessee",
    "Austin": "Texas"
}

const State = [...new Set(Object.values(CityState))];
const Cities = [...new Set(Object.keys(CityState))];

// makes CityState into a list of strings with format
// "City, State"
CityState = Object.keys(CityState).map((cityName) => {
    return `${cityName}, ${CityState[cityName]}`
});


const Causes = [
    "Helping/Aid",
    "Science and Technology Research Institutes, Services",
    "Medical Research",
    "Educational Institutions and Related Activities",
    "Civil Rights, Social Action, Advocacy",
    "Public Safety, Disaster Preparedness and Relief",
    "Mutual/Membership Benefit Organizations, Other",
    "Health - General and Rehabilitative",
    "Youth Development",
    "Employment, Job-Related",
    "Philanthropy, Voluntarism and Grantmaking Foundations",
    "Arts, Culture and Humanities",
    "Mental Health, Crisis Intervention",
    "Diseases, Disorders, Medical Disciplines",
    "Human Services - Multipurpose and Other",
    "International, Foreign Affairs and National Security",
    "Animal-Related",
    "Food, Agriculture and Nutrition",
    "Social Science Research Institutes, Services",
    "Crime, Legal-Related",
    "Public, Society Benefit - Multipurpose and Other",
    "Religion-Related, Spiritual Development",
    "Environmental Quality, Protection and Beautification",
    "Community Improvement, Capacity Building",
    "Housing, Shelter",
    "Recreation, Sports, Leisure, Athletics"
]

export { CityState, Causes, State, Cities };