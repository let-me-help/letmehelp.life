import React, { Component } from 'react';
import { Organizer } from './Organizer';
import Pagination from './Pagination';
import { getUrlParamStr } from '../../tools/StringTools';

const SORT_TRANSLATION = {
  "A-Z": "asc",
  "Z-A": "desc",
  "newer": "desc",
  "older": "asc",
  "higher": "desc",
  "lower": "asc"
};

function  getTranslatedSelections(selections) {
  const trans = {};
  const sorts = [];
  for (const id in selections) {
    const config = selections[id];
    switch(config.type) {
      case "search":
        if (config.value.length) {
          trans[config.id] = config.value;
        }
        break;
      case "sort":
        if(config.value && config.value !== "None") {
          sorts.push(`${config.id}:${SORT_TRANSLATION[config.value]}`);
        }
        break;
      case "filter":
        if (config.value) {
          // TODO(daniel): change slider into its own type.
          if (config.value.length === 2) {
            trans[config.id] = config.value.join("-");
            break;
          }
          const value = config.id === "date" ?
            config.value._d.toISOString() :
            config.value;
          trans[config.id] = value;
        }
        break;
      default:
        console.log("Excuse me, wtf is this? ");
        console.log(config);
    }
  }
  if (sorts.length) {
    trans["sort"] = sorts.join(",");
  }
  return trans;
}

class OrganizedPagination extends Component {

  constructor(props) {
    super(props);
    this.state = {q: undefined, apiEndpoint: this.props.apiEndpoint};
  }

  onSelect(selections) {
    const translatedSelections = getTranslatedSelections(selections);
    this.setState(
      {
        apiEndpoint:
          `${this.props.apiEndpoint}?${getUrlParamStr(translatedSelections)}`,
        q: selections.q.value === '' ? undefined : selections.q.value
      }
    );
  }

  render() {
    return (
    <div className="container">
      <a className="btn btn-secondary" data-toggle="collapse" 
        href="#collapse" role="button" aria-expanded="false" 
        aria-controls="collapse">
        Advanced Options
      </a>
      <div className="collapse" id="collapse">
        <br/>
        <Organizer
          organizerSettings={this.props.organizerSettings}
          onSelect={this.onSelect.bind(this)}
        />
      </div>
      <hr/>
      <Pagination
        rowCount={this.props.rowCount}
        colCount={this.props.colCount}
        apiEndpoint={this.state.apiEndpoint}
        cardFn={this.props.createCardFn(this.state.q)}
      />
    </div>
    );
  }
}

export default OrganizedPagination;
