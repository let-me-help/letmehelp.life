import React, { Component } from 'react';
import axios from 'axios';
import { PulseLoader } from 'react-spinners';
import { BASE_API_URL } from '../../tools/constants';
import Row from '../general/Row';


const LOAD_MSG = <PulseLoader
                    sizeUnit={"vh"}
                    size={1}
                    color={'#f8bbd0'}
                    loading={true}
                 />;
const MORE_MSG = "Load More";
const FINISHED_MSG = "You've reached the end!";
const EMPTY_MSG = <div>No results found{" "}
                    <span
                      role="img"
                      aria-label="crying face">
                        😢
                    </span>
                  </div>;

const CENTER_DIV = {
  display:"flex",
  justifyContent:"center",
  alignItems:"flex-end"
};

function getInitialState() {
  return {
    activePages: 0,
    loading: false,
    buttonMsg: LOAD_MSG,
    items: [],
    rows: []
  };
}

class Pagination extends Component {

  constructor(props) {
    super(props);
    this.pageSize = props.rowCount * this.props.colCount;
    this.state = getInitialState();
  }

  componentDidMount() {
    this.loadPage();
  }

  componentDidUpdate(prevProps, prevState) {
    const pageUpdated = this.state.activePages !== prevState.activePages;
    const endpointUpdated = this.props.apiEndpoint !== prevProps.apiEndpoint;
    if (pageUpdated && !endpointUpdated) {
      this.updateRows();
    } else if (endpointUpdated) {
      this.setState(getInitialState(), this.loadPage);
    }
  }

  loadPage() {
    const partialUrl = `${BASE_API_URL}${this.props.apiEndpoint}`;
    const connector = partialUrl.includes('?') ? '&' : '?';
    const offset = this.state.activePages * this.pageSize;
    const fullUrl =
      `${partialUrl}${connector}max_results=${this.pageSize}&offset=${offset}`;
    if (!this.state.loading) {
      this.setState(
        { loading: true,
          buttonMsg: LOAD_MSG },
        () => { axios.get(fullUrl)
                  .then(
                    (response) =>
                      {
                        this.setState(
                          { items: [...this.state.items , ...response.data],
                            activePages: this.state.activePages + 1,
                            loading: false,
                            buttonMsg: MORE_MSG },
                          this.updateRows);
                      });
              });
    }
  }

  updateRows() {
    if (this.state.items.length < 1) {
      this.setState({ buttonMsg: EMPTY_MSG });
      return;
    };
    const { rowCount, colCount } = this.props;
    const { items, activePages, rows } = this.state;
    // Zero-based active page
    const start = (activePages - 1) * this.pageSize;
    const end = start + this.pageSize;
    const pageItems = items.slice(start, end);
    // Check if there are any more items to display.
    if (pageItems.length < 1) {
      this.setState({ buttonMsg: FINISHED_MSG });
      return;
    }
    // Apply function to responses to create page cards.
    const pageComponents = pageItems.map(this.props.cardFn);
    // Fill pageComponents to at least 1 page.
    while (pageComponents.length < this.pageSize) {
      pageComponents.push(null);
    }
    // Generate rows for the new page.
    let sliceIdx = 0;
    const newRows = [];
    for (let r = 0; r < rowCount; r++) {
      const columns = pageComponents
        .slice(sliceIdx, sliceIdx + colCount)
        .filter((col) => !!col);
      newRows.push(<Row
                      key={r}
                      columnType="col-md-3 col-sm-6 mb-4 card-group">
                        {columns}
                   </Row>);
      sliceIdx += colCount;
    }
    this.setState({
      'rows': [...rows, ...newRows]
    });
  }

  render() {
    return (
    <div className="container">
      {this.state.rows}
      <div style={CENTER_DIV}>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={this.loadPage.bind(this)}>
            {this.state.buttonMsg}
        </button>
      </div>
      <br/>
    </div>
    );
  }
}

export default Pagination;
