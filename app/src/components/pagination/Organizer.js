import React, { Component } from 'react';
import Row from '../general/Row';
import {Typeahead} from 'react-bootstrap-typeahead';
import DatePicker from "react-datepicker";
import Slider from 'rc-slider';
import "react-datepicker/dist/react-datepicker.css";
import 'rc-slider/assets/index.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead-bs4.css';

const SelectorTypes = Object.freeze({
    DROPDOWN:   "dropDown",
    RADIOBUTTON:  "radioButton",
    TYPEAHEAD: "typeAheadSearch",
    DATEPICKER: "datePicker",
    RANGESLIDER: "rangeSlider"
});

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

const dropDowns = (dropdowns, callback, orgType) => {
    return (dropdowns.map((criteria, k) => {
        const criteriaName = criteria.label;
        const criteriaValues = criteria.vals.map((val, k) =>
            <button key={k} onClick={(e) => callback(e, {
                    'value': val, 
                    'id': criteria.id,
                    'type': orgType
                })}
                className="dropdown-item" type="button">
                {val}
            </button>
        );
        return (
            <div key={k} className="dropdown">
                <button className="btn btn-secondary dropdown-toggle"
                    type="button" id={`${criteriaName}Select`}
                    data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                {criteriaName}
                </button>
                <div className="dropdown-menu"
                    aria-labelledby={`${criteriaName}Select`}>
                    {criteriaValues}
                </div>
            </div>
        );
    }));
}

const radioButtons = (radiobuttons, callback, orgType) => {
    return (radiobuttons.map((criteria, k) => {
        const criteriaName = criteria.label;
        const criteriaValues = criteria.vals.map((val, k) =>
            <label onClick={(e) => callback(e, {
                    'value': val, 
                    'id': criteria.id,
                    'type': orgType
                })}
                key={k} className="btn btn-secondary">
                <input
                    type="radio" name="options" id={`${val}`}
                    autoComplete="off"/> {val}
            </label>
        );
        return (
        <div key={k}>
            <label htmlFor={`${criteriaName}`}>{`${criteriaName}`}</label>
            <br/>
            <div id={`${criteriaName}`}
                className="btn-group btn-group-toggle"
                data-toggle="buttons">
                {criteriaValues}
            </div>
        </div>);
    }));
}

const typeAheadSearch = (search, callback, orgType) => {
    const searches = search.map((criteria ,k) =>
        <Typeahead key={k}
            maxResults={5}
            placeholder={`Search by ${criteria.label}`}
            onChange={(selected) => callback({}, {
                'value': (selected[0] !== undefined && 
                        selected[0].id !== undefined) ? 
                        selected[0].id : selected[0],
                'id': criteria.id,
                'type': orgType
            })}
            options={criteria.vals}/>
    );
    return searches;
}

const datePicker = (pickers, callback, state, orgType) => {
    const datepickers = pickers.map((criteria, k) =>
        <DatePicker className="form-control" key={k}
        onChange={(selected, e) => {
            selected._d.setUTCHours((new Date().getTimezoneOffset())/60);
            selected._d.setUTCMinutes(0);
            selected._d.setUTCSeconds(0);
            callback(e, {
                'value': selected, 
                'id': criteria.id,
                'type': orgType
            });
        }}
        selected={state[`${criteria.id}${orgType}`] !== undefined ? 
            state[`${criteria.id}${orgType}`].value : undefined}
        calendarClassName="rasta-stripes"
        placeholderText="Select a date"
        dateFormat="MM/DD/YYYY"
        />
    );
    return datepickers;
}
  
const rangeSlider = (sliders, callback, orgType) => {
    const rangesliders = sliders.map((criteria, k) => 
        <div key={k}>
            <label htmlFor={`${criteria.id}Slider`}>
                {`${criteria.label}`}</label>
            <br/>
            <Range id={`${criteria.id}Slider`}
                onAfterChange={(e) => callback({}, {
                    'value': e, 
                    'id': criteria.id,
                    'type': orgType
                })}
                defaultValue={[criteria.vals.min, criteria.vals.max]} 
                min={criteria.vals.min}
                max={criteria.vals.max}
                step={criteria.vals.step}
                trackStyle={[{'backgroundColor': '#81c784'}]}
                handleStyle={[{'backgroundColor': '#f06292', 'borderColor': '#f06292'}]}
                allowCross={false}/>
        </div>
    );
    return rangesliders;
}

const optionsMenu = (options, callback, state, orgType) => {
    const menuDisplay = Object.keys(options).map((optType, k) => {
        switch(optType){
            case SelectorTypes.DROPDOWN:
                return dropDowns(options[optType], callback, orgType);
            case SelectorTypes.RADIOBUTTON:
                return radioButtons(options[optType], callback, orgType);
            case SelectorTypes.TYPEAHEAD:
                return typeAheadSearch(options[optType], callback, orgType);
            case SelectorTypes.DATEPICKER:
                return datePicker(options[optType], callback, state, orgType);
            case SelectorTypes.RANGESLIDER:
                return rangeSlider(options[optType], callback, orgType);
            default:
                return <div></div>
        }
    });
    return menuDisplay;
}

class Organizer extends Component {
    constructor(props) {
        super(props);
        this.handleCriteriaChange = this.handleCriteriaChange.bind(this);
        // by default all organizers will have a search term in their state
        // this is because all of our models require a search feature
        // in their respective pages. Other values in the state can be
        // added whenever their values are inputted by user.
        this.state = {
            "q": {
                'value': "",
                'id': "q",
                'type': "search"
            },
        };
    }

    handleCriteriaChange(e, response) {
        let update = {};
        // had to change the index into the update in order to allow
        // filters and sorters of the same attribute to exist in 
        // one Organizer configuration
        // used to be update[response.id]
        update[`${response.id}${response.type}`] = response;
        this.setState(update);
    }

    handleSearch(e) {
        if (e.key === 'Enter') {
            this.setState({"q":  {
                'value': encodeURIComponent(e.target.value),
                'id': "q",
                'type': "search"
            }});
        }

    }

    componentDidUpdate(prevProps, prevState) {
        const stateUpdate = JSON.stringify(prevState) !== JSON.stringify(this.state);
        if (stateUpdate) {
            this.props.onSelect(this.state);
        }
    }

    render() {
        // OrganizerSettings object
        const organizerSettings = this.props.organizerSettings;
        const filterOpt = [].concat.apply([], optionsMenu(organizerSettings.filter, 
                        this.handleCriteriaChange, this.state, "filter"));
        const sortOpt =  [].concat.apply([], optionsMenu(organizerSettings.sort, 
                        this.handleCriteriaChange, this.state, "sort"));

        return (
        <div className="container">
            <Row columnType="col-auto col-bottom-pad-20">
                {sortOpt}
            </Row>
            <Row columnType="col-md-3 col-sm-6 mb-4">
                    <input className="form-control"
                        type="text"
                        onKeyPress={(e) => this.handleSearch(e)}
                        placeholder={`Search for ${organizerSettings.name}`}/>
            </Row>
            <Row columnType="col-md-3 col-sm-6 mb-4">
                   {filterOpt}
            </Row>
            <button type="button" 
                className="btn btn-secondary"
                onClick={(e) => window.location.reload()}>
                Reset
            </button>
        </div>);
    }
}


export {Organizer, SelectorTypes};