import React from 'react';
import jest from 'jest';
import { shallow } from 'enzyme';
import OrganizedPagination from './OrganizedPagination';
import Card from '../general/Card';

function createTestCard(response) {
  return (
      <Card
          key="test"
          header={
            <p>test</p>
          }
          body={[<p>test</p>, <p>test</p>]}
      />);
}
describe('OrganizedPagination', () => {

  // @author Daniel
  it('renders correctly', () => {
    const wrapper = shallow(<OrganizedPagination 
                        createCardFn={createTestCard}/>);
    expect(wrapper).toMatchSnapshot();
  });

});
