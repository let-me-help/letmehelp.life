import React from 'react';
import { shallow } from 'enzyme';
import { Organizer, SelectorTypes } from './Organizer';
import OrganizerSettings from './OrganizerSettings';


describe('Organizer', () => {
    it('renders correctly', () => {
        const settings = new OrganizerSettings("Test");
        settings.addSort(SelectorTypes.RADIOBUTTON, 'Name', 
            ['A-Z', 'Z-A'], 'nameSort');
        settings.addFilter(SelectorTypes.DATEPICKER, 
            'Date', [], 'selectedDate'); 
        settings.addFilter(SelectorTypes.RANGESLIDER, 
            'Poverty Rate', {'min': 0, 'max': 20, 'step': 1}, 
            'povertyRateRange');
        settings.addFilter(SelectorTypes.DROPDOWN, 
            'Test', ["a", "b"], 
            'test1');
        settings.addFilter(SelectorTypes.TYPEAHEAD, 
            'Test2', ["a", "b"], 
            'test2');
        const wrapper = shallow(<Organizer organizerSettings={settings}/>);

        expect(wrapper).toMatchSnapshot();
    });


});