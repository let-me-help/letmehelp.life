import React from 'react';
import { shallow } from 'enzyme';
import Carousel from './Carousel';
import CarouselSlide from './CarouselSlide';

describe('Carousel Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<Carousel name="TestCarousel"></Carousel>);
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains the correct amount of slides', () => {
        const wrapper = shallow(
            <Carousel name="TestCarousel">
                <CarouselSlide active img="" caption="cap1"/>
                <CarouselSlide img="" caption="cap2"/>
            </Carousel>
            );
        expect(wrapper.find('div.carousel-inner')
                      .children().length).toEqual(2);
    });
});