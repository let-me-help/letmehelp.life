import React from 'react';
import { shallow } from 'enzyme';
import Row from './Row';


describe('Row Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(
        <Row columnType="col-md-4">
            <h1>First child</h1>
            <p>second child</p>
        </Row>);
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains all children', () => {
        const wrapper = shallow(
            <Row columnType="col-md-4">
                <h1>First child</h1>
                <p>second child</p>
            </Row>);
        expect(wrapper.children().length).toEqual(2)
    });

    // @Author Jorge
    it('contains the correct column types', () => {
        const wrapper = shallow(
            <Row columnTypes={["col-md-4", "col-md-2"]}>
                <h1>First child</h1>
                <p>second child</p>
            </Row>);
        expect(wrapper.find('div.col-md-4').exists())
            .toBeTruthy();
        expect(wrapper.find('div.col-md-2').exists())
            .toBeTruthy();    
    });
});