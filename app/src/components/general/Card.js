import React, { Component } from 'react';
import posed from 'react-pose';

const BOX_SHADOW = "boxShadow";
const SHADOW_STYLE = '0px 2px 10px black';
const HIDDEN_SHADOW_STYLE = '0px 0px 0px black';

function getHoverDiv(shadow, hoverShadow) {
  const poseOptions = {
    idle: {
      scale: 1
    },
    hovered: {
      scale: 1.2
    }
  };
  if (shadow) {
    poseOptions.idle[BOX_SHADOW] = SHADOW_STYLE;
    poseOptions.hovered[BOX_SHADOW] = HIDDEN_SHADOW_STYLE;
  } else if (hoverShadow) {
    poseOptions.idle[BOX_SHADOW] = HIDDEN_SHADOW_STYLE;
    poseOptions.hovered[BOX_SHADOW] = SHADOW_STYLE;
  }
  return posed.div(poseOptions);
}

class Card extends Component {

  constructor(props) {
    super(props);
    this.HoverDiv = getHoverDiv(this.props.shadow, this.props.hoverShadow);
    this.state = { hovering: false };
  }

  render() {
    const shouldHover = this.props.hover && this.state.hovering;
    const HoverDiv = this.HoverDiv;
    let header = this.props.header;
    if (shouldHover && this.props.hoverHeader) {
      header = this.props.hoverHeader;
    }
    let body = this.props.body;
    if (shouldHover && this.props.hoverBody) {
      body = this.props.hoverBody;
    }
    if (body && body.constructor === Array) {
      const newBody = []
      for (let i = 0; i < body.length; i++) {
        newBody.push(<div className="card-text" key={i}>{body[i]}</div>);
      }
      body = newBody;
    }
    return (
      <HoverDiv
        className="card mb-3"
        pose={shouldHover ? "hovered" : "idle"}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
        onTouchStart={() => this.setState({ hovering: true })}
        onTouchEnd={() => this.setState({ hovering: false })}
        >
        <div className="card-header">
          {header}
        </div>
        <div className="card-body">
          {body}
        </div>
      </HoverDiv>
    );
  }
}

export default Card;
