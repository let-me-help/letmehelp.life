import React from 'react';
import { shallow } from 'enzyme';
import ImageCard from './ImageCard';

describe('Image Card component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<ImageCard image="" imageTitle="title1"
                                    imageSubTitle="subtitle1"
                                    body={[
                                        <p>fact 1</p>,
                                        <p>fact 2</p>,
                                        <p>fact 3</p>
                                    ]}
                                    footer="footer1"/>);
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains correct body', () => {
        const wrapper = shallow(<ImageCard image="" imageTitle="title1"
                                    imageSubTitle="subtitle1"
                                    body={<h1>body1</h1>}
                                    footer="footer1"/>);
        expect(wrapper.find('h1').text()).toEqual("body1");
    });

    // @Author Jorge
    it('body contains the correct amount of children', () => {
        const wrapper = shallow(<ImageCard image="" imageTitle="title1"
                                    imageSubTitle="subtitle1"
                                    body={[
                                        <p>fact 1</p>,
                                        <p>fact 2</p>,
                                        <p>fact 3</p>
                                    ]}
                                    footer="footer1"/>);
        expect(wrapper.find('div.card-text').length).toEqual(3);
    });
});