import React from 'react';
import { shallow } from 'enzyme';
import BreadCrumb  from './BreadCrumb';


describe('BreadCrumb', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<BreadCrumb crumbs={["a", "b"]}
                                        links={["/"]}/>);
        expect(wrapper).toMatchSnapshot();
    });
});