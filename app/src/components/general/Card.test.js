import React from 'react';
import { shallow } from 'enzyme';
import Card from './Card';


describe('Card Component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<Card header={<h2>Test</h2>}
                                    body={<p>Test body</p>}/>);
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains a header and body', () => {
        const wrapper = shallow(<Card header={<h2>Test</h2>}
                                body={<p>Test body</p>}/>);
        const header = wrapper.find('div.card-header').find('h2').text();
        const body = wrapper.find('div.card-body').find('p').text();

        expect(header).toEqual('Test');
        expect(body).toEqual('Test body');
    });

    it('contains all body children', () => {
        const wrapper = shallow(<Card header={<h2>Test</h2>}
                            body={[
                                <p>Test body 1</p>,
                                <h1>Test body 2</h1>
                            ]}/>);
        expect(wrapper.contains([
            <div className="card-text">
                <p>Test body 1</p>
            </div>,
            <div className="card-text">
                <h1>Test body 2</h1>
            </div>
        ])).toBeTruthy();
    });
});