import React, { Component } from 'react';

class Row extends Component {

  render() {
    let columnTypes = this.props.columnTypes;
    let children = this.props.children;
    if (!Array.isArray(children)){
      children = [children];
    }
    if (!columnTypes) {
      columnTypes = children.map((_) => this.props.columnType);
    }
    const columns = children;
    const columnComponents = [];
    for(let i = 0; i < columns.length; i++) {
      columnComponents.push(<div key={i} className={columnTypes[i]}>{columns[i]}</div>);
    }
    return (
      <div className="row">
        {columnComponents}
      </div>);
  }
}

export default Row;
