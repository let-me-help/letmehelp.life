import React from 'react';
import { shallow } from 'enzyme';
import GoogleMap from './GoogleMap';

describe('GoogleMap Component', () => {
    // @Author Jorge
    it('render correctly', () => {
        const wrapper = shallow(<GoogleMap qry="Austin+TX"/>);
        expect(wrapper).toMatchSnapshot();
    });
});