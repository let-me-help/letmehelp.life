import React, { Component } from 'react';
import '../../bootstrap/css/full-slider.css'


class CarouselSlide extends Component {
    render(){
        const {img, caption, active} = this.props; 
        let activeClass = active ? "carousel-item active" : "carousel-item"
        return (
            <div className={activeClass} style={{backgroundImage: `url(${img})` }}>
                <div className="carousel-caption d-none d-md-block">
                    <h3> {caption} </h3>
                </div>
            </div>
        );
    }
}

export default CarouselSlide;