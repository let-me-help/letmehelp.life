import React, { Component } from 'react';

class Carousel extends Component {
    render() {
        const { name } = this.props
        return (
            <div id={name} className="carousel slide" data-ride="carousel">
                <div className="carousel-inner" role="listbox" data-interval="1000">
                    { this.props.children }
                </div>
                <a className="carousel-control-prev" role="button" data-slide="prev" href={`#${name}`}>
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" role="button" data-slide="next" href={`#${name}`}>
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        );
    }
}

export default Carousel;