import React from 'react';
import { shallow } from 'enzyme';
import SocialMediaFeed from './SocialMediaFeed';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import { FacebookProvider, Page } from 'react-facebook';

describe('SocialMediaFeed', () =>{
    
    //@Author Jorge
    it('renders correctly', () => {
        const accounts = {"facebook": "facebook" , "twitter": "Twitter"};
        const wrapper = shallow(<SocialMediaFeed accounts={accounts}/>);

        expect(wrapper).toMatchSnapshot();
    });

    //@Author Jorge
    it('contains a Facebook and Twitter component', () => {
        const accounts = {"facebook": "facebook" , "twitter": "Twitter"};
        const wrapper = shallow(<SocialMediaFeed accounts={accounts}/>);

        expect(wrapper.find(FacebookProvider)).toBeTruthy();
        expect(wrapper.find(TwitterTimelineEmbed)).toBeTruthy();
    });

    //@Author Jorge
    it('contains the correct amount of tabs', () => {
        const accounts = {"facebook": "facebook"};
        const wrapper = shallow(<SocialMediaFeed accounts={accounts}/>);

        expect(wrapper.find('#facebook-tab')).toBeTruthy();
        // test 0
        expect(wrapper.find('#twitter-tab').length).toBeFalsy();
    });
});