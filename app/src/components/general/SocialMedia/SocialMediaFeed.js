import React, { Component } from 'react';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import { FacebookProvider, Page } from 'react-facebook';

const feed = (socialMedia, accName) => {
    if (socialMedia === "twitter"){
       return( <TwitterTimelineEmbed
            sourceType="profile"
            screenName={accName}
            options={{height: 500}}/>);
    } else if (socialMedia === "facebook"){
        return (  
            <FacebookProvider appId="312593516228164">
                <Page href={`https://www.facebook.com/${accName}/`} 
                    tabs="timeline" />
            </FacebookProvider>    
        );
    }
}

const socialMediaTab = (socialMedia, active, k) => (
    <li key={k} className="nav-item">
        <a className={`nav-link ${active}`} id={`${socialMedia}-tab`} 
            data-toggle="tab" href={`#${socialMedia}`} role="tab" 
            aria-controls={`${socialMedia}`} aria-selected="false">
            {socialMedia.split(" ")
                        .map((s) => {
                            return s.charAt(0).toUpperCase() + s.substring(1)
                        })}</a>
    </li>
);

const socialMediaTabContent = (socialMedia, accName, active, k) => (
    <div key={k} className={`tab-pane fade show ${active}`}
        id={`${socialMedia}`} role="tabpanel" 
        aria-labelledby={`${socialMedia}-tab`}>
        {feed(socialMedia, accName)}
    </div>
);

const socialMediaTabs = (accs) => {
    let tabs = [];
    let tabContent = [];
    Object.keys(accs).forEach( (socialMedia, idx) => {
        if (accs[socialMedia] !== undefined && accs[socialMedia] !== "" 
            && (socialMedia === "facebook" || socialMedia === "twitter")) {
            const active = idx === 0 ? "active" : ""
            tabs.push(socialMediaTab(socialMedia, active, idx));
            tabContent.push(socialMediaTabContent(socialMedia, accs[socialMedia], active, idx));
        }
    });
    return {"tabs": tabs, "tabContent": tabContent}
}

class SocialMediaFeed extends Component {
    render() {
        const accs = this.props.accounts
        const {tabs, tabContent} = socialMediaTabs(accs);
        return (
            <div id="socialMedia">
                <ul className="nav nav-tabs">
                    {tabs}
                </ul>
                <div className="tab-content" id="socialMediaContent">
                    {tabContent}
                </div>
            </div>
        );
    }
}


export default SocialMediaFeed