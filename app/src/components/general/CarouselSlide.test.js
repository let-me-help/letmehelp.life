import React from 'react';
import { shallow } from 'enzyme';
import CarouselSlide from './CarouselSlide';

describe('Carousel Slide component', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow(<CarouselSlide img="" caption="cap1"/>);
        expect(wrapper).toMatchSnapshot();
    });

    // @Author Jorge
    it('contains correct caption', () => {
        const wrapper = shallow(<CarouselSlide img="" caption="cap1"/>);
        expect(wrapper.find('div.carousel-caption.d-none.d-md-block')
                      .find('h3').text()).toEqual(' cap1 ');
    });

    // @Author Jorge
    it('contains the active class', () => {
        const wrapper = shallow(<CarouselSlide active img="" caption="cap1"/>);
        expect(wrapper.find('div.carousel-item.active')).toBeTruthy();
    });
});