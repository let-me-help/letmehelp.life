import React, { Component } from 'react';

class ImageCard extends Component {

  render() {
  	let body = this.props.body;
  	if (body && body.constructor === Array) {
  		body = body.map((item, k) => <div key={k} className="card-text">{item}</div>);
  	}
    return (
      <div className="card h-100 text-center">
        <img className="card-img-top" src={this.props.image} alt=""></img>
        <div className="card-body">
          <h4 className="card-title">{this.props.imageTitle}</h4>
          <h6 className="card-subtitle mb-2 text-muted">{this.props.imageSubtitle}</h6>
          {body}
        </div>
        <div className="card-footer">
          {this.props.footer}
        </div>
      </div>);
  }
}

export default ImageCard;
