import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class BreadCrumb extends Component {

  render() {
    const crumbs = [];
    const numCrumbs = this.props.crumbs.length;
    for(let i = 0; i < numCrumbs - 1; i++) {
    	crumbs.push(
	    	<li key={i} className="breadcrumb-item">
	         <Link to={this.props.links[i]}>{this.props.crumbs[i]}</Link>
	      </li>);
    }
    crumbs.push(
    	<li key="last" className="breadcrumb-item active">
    		{this.props.crumbs[numCrumbs - 1]}
    	</li>);

    return (
      <ol className="breadcrumb">
      	{crumbs}
      </ol>);
  }
}

export default BreadCrumb;
