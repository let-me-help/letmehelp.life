import React from 'react';
import { shallow } from 'enzyme';
import CitiesEventsChart from './CitiesByEventCount';

import { EventCountCities } from '../../tools/constants';


describe('Event Count in Cities', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow( <CitiesEventsChart 
            width={540} height={540}
            radius={180} data={EventCountCities}/>);
        
        expect(wrapper).toMatchSnapshot();
    });
});