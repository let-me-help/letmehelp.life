import React, { Component } from 'react';
import * as d3 from 'd3';
import { withFauxDOM } from 'react-faux-dom';

class CitiesPopAndPoverty extends Component {

    componentDidMount() {
        const faux = this.props.connectFauxDOM('svg', 'chart');

        const svg = d3.select(faux),
            margin = {top: 20, right: 20, bottom: 30, left: 40},
            width =  this.props.width - margin.left - margin.right,
            height = this.props.height - margin.top - margin.bottom,
            g = svg.attr("viewBox", `0 0 ${this.props.width} ${this.props.height}`)
                .attr("preserveAspectRatio", "xMidYMid meet")
                .append("g")
                .attr("transform", 
                "translate(" + margin.left + "," + margin.top + ")");
        const x0 = d3.scaleBand()
            .rangeRound([0, width])
            .paddingInner(0.1);
        const x1 = d3.scaleBand()
            .padding(0.05);
        const y = d3.scaleLinear()
            .rangeRound([height, 0]);
        const z = d3.scaleOrdinal()
            .range(["#98abc5", "#8a89a6", "#7b6888", 
                "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
    
        const keys = [ "City", "Population", "People living below poverty line" ]
                .slice(1).slice(0,2);
        x0.domain(this.props.data.map((d) => d.City));
        x1.domain(keys).rangeRound([0, x0.bandwidth()]);
        y.domain([0, d3.max(this.props.data, (d) => d3.max(keys, (key) => d[key]))])
            .nice();

        g.append("g")
            .selectAll("g")
            .data(this.props.data)
            .enter().append("g")
            .attr("transform", (d) => "translate(" + x0(d.City) + ",0)")
            .selectAll("rect")
            .data((d) => keys.map((key) => {return {key: key, value: d[key]}; }))
            .enter().append("rect")
            .attr("x", (d) => x1(d.key))
            .attr("y", (d) => y(d.value))
            .attr("width", x1.bandwidth())
            .attr("height", (d) => height - y(d.value))
            .attr("fill", (d) => z(d.key));
        g.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x0));
        g.append("g")
            .attr("class", "axis")
            .call(d3.axisLeft(y).ticks(null, "s"))
            .append("text")
            .attr("x", 2)
            .attr("y", y(y.ticks().pop()) + 0.5)
            .attr("dy", "0.32em")
            .attr("fill", "#000")
            .attr("font-weight", "bold")
            .attr("text-anchor", "start")
            .text("Number of People(Year: 2016)");
        const legend = g.append("g")
            .attr("font-family", "sans-serif")
            .attr("font-size", 10)
            .attr("text-anchor", "end")
            .selectAll("g")
            .data(keys.slice().reverse())
            .enter().append("g")
            .attr("transform", (d, i) => "translate(0," + i * 20 + ")");
        legend.append("rect")
            .attr("x", width - 19)
            .attr("width", 19)
            .attr("height", 19)
            .attr("fill", z);
        legend.append("text")
            .attr("x", width - 24)
            .attr("y", 9.5)
            .attr("dy", "0.32em")
            .text((d) => d);
    }

    render() {
        return (
            <div className="renderedD3">
                {this.props.chart}
            </div>
        );
    }
}

export default withFauxDOM(CitiesPopAndPoverty);