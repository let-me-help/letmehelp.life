import React, { Component } from 'react';
import * as d3 from 'd3';
import { withFauxDOM } from 'react-faux-dom';


class CitiesByEventCount extends Component {

    componentDidMount() {
        const faux = this.props.connectFauxDOM('div', 'chart');
        
        const colors = d3.scaleOrdinal(d3.schemePaired);

        const arc = d3.arc()
            .outerRadius(this.props.radius - 10)
            .innerRadius(100);

        const pie = d3.pie()
            .sort(null)
            .value((d) => d.numEvents);

        const svg = d3.select(faux).append("svg")
            .attr("viewBox", `0 0 ${this.props.width} ${this.props.height}`)
            .attr("preserveAspectRatio", "xMidYMid meet")
            .append("g")
            .attr("transform", "translate(" + this.props.width / 2 + "," + 
                    this.props.height / 2 + ")");

        const g = svg.selectAll(".arc")
            .data(pie(this.props.data))
            .enter().append("g");

        g.append("path")
            .attr("d", arc)
            .attr("fill", (d) => colors(d.data.city) );

        g.append("text")
            .attr("transform", (d) => {
                var _d = arc.centroid(d);
                _d[0] *= 1;
                _d[1] *= 1;
                return "translate(" + _d + ")";
            })
            .attr("dy", ".50em")
            .style("text-anchor", "middle")
            .text((d) => d.data.numEvents)
            .attr("fill", "white");

        g.append("text")
            .attr("transform", (d) => {
                var _d = arc.centroid(d);
                _d[0] *= 1.6;
                _d[1] *= 1.5;
                return "translate(" + _d + ")";
            })
            .attr("dy", ".50em")
            .style("text-anchor", "middle")
            .text((d) => d.data.city)
            .attr("fill", (d)  => colors(d.data.city) );

        svg.append("text")
            .attr("transform", "translate(" + 0 + "," + -20 + ")")
            .style("text-anchor", "middle")
            .attr("font-size", "1.9em")
            .text("Total")
            .attr("fill", "#81c784");

        svg.append("text")
            .attr("transform", "translate(" + 0 + "," + 30 + ")")
            .style("text-anchor", "middle")
            .attr("font-size", "3.5em")
            .text("816")
            .attr("fill", "#81c784");

        svg.append("text")
            .attr("transform", "translate(" + 0 + "," + -240 + ")")
            .style("text-anchor", "middle")
            .attr("font-size", "1.2em")
            .attr("font-weight", "bold")
            .attr("fill", "#81c784")
            .text("TOP 10 CITIES BASED ON EVENTS COUNT");
    }

    render() {
        return (
            <div className="renderedD3">
                {this.props.chart}
            </div>
        );
    }
}

export default withFauxDOM(CitiesByEventCount);