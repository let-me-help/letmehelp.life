import React, { Component } from 'react';
import * as d3 from 'd3';

import { uStatePaths, StateAbbrv } from '../../tools/constants';

const USAMap = () => {
    const uStates={};
    uStates.draw = (id, data, toolTip) => {	
        const valid_states = ['KY', 'OK', 'FL', 'MI', 'NV', 'IN', 'WA', 'MD', 'PA', 'OH',
            'MA', 'NC', 'TN', 'AZ', 'IL', 'NY', 'DC', 'CO', 'CA', 'TX', 'OR'];
        const mouseOver = (d) => {
            if (valid_states.indexOf(d.id) >= 0) {
                d3.select("#tooltip")
                    .transition()
                    .duration(200)
                    .style("opacity", .9);      
                d3.select("#tooltip")
                    .html(toolTip(d.n, data[d.id]))  
                    .style("left", (d3.mouse(id)[0]) + "px")
                    .style("top", (d3.mouse(id)[1] - 28) + "px");
            }
        }
    

        const mouseOut = () => {
            d3.select("#tooltip")
                .transition()
                .duration(500)
                .style("opacity", 0);
        }

        d3.select("#statesvg").selectAll(".state")
            .data(uStatePaths)
            .enter().append("path")
            .attr("class","state").attr("d",(d) => d.d)
            .style("fill",(d) => data[d.id].color)
            .on("mouseover", mouseOver).on("mouseout", mouseOut);
    }
    return uStates;
}

const tooltipHtml = (n, d) => {	/* function to create html content string in tooltip div. */

    return "<h4>"+n+"</h4><table>"+
		"<tr><th>Charities</th><td>"+(d.charities)+"</td></tr>"+
		"<tr><th>Cause</th><td>"+(d.cause)+"</td></tr>"+
		"<tr><th>Events</th><td>"+(d.events)+"</td></tr>"+
		"</table>";
}

class StateInformationMap extends Component {
    componentDidMount() {
        // D3 Code to create the chart
        // using this._rootNode as container
        const uStates = USAMap();
        StateAbbrv.forEach((d) => {
            this.props.data[d]['color'] = d3.interpolate("#FFFFFF", 
                            "#81c784")(this.props.data[d].charities/90); 
        });
        d3.select(this._rootNode)
            .append("svg")
            .attr("viewBox", `0 0 ${this.props.width} ${this.props.height}`)
            .attr("preserveAspectRatio", "xMidYMid meet")
            .attr("id", "statesvg");

        uStates.draw(this._rootNode, this.props.data, tooltipHtml);
    }

    shouldComponentUpdate() {
        return false;
    }

    _setRef(componentNode) {
        this._rootNode = componentNode;
    }

    render() {
        return (
            <div className="line-container" 
                ref={this._setRef.bind(this)}>
                <style>
                    {
                    `
                        .state{
                            margin-top:60px;
                            fill: none;
                            stroke: #a9a9a9;
                            stroke-width: 1;
                        }
                        .state:hover{
                            fill-opacity:0.5;
                        }
                        #tooltip {   
                            position: absolute;           
                            text-align: center;
                            padding: 20px;             
                            margin: 10px;
                            font: 12px sans-serif;        
                            background: lightsteelblue;   
                            border: 1px;      
                            border-radius: 2px;           
                            pointer-events: none;         
                        }
                        #tooltip h4{
                            margin:0;
                            font-size:14px;
                        }
                        #tooltip{
                            background:#f06292;
                            border:1px solid grey;
                            border-radius:5px;
                            font-size:12px;
                            width:auto;
                            padding:4px;
                            color:white;
                            opacity:0;
                        }
                        #tooltip table{
                            table-layout:fixed;
                        }
                        #tooltip td{
                            padding:0;
                            margin:0;
                            border: .1px solid rgba(220, 220, 220, .5);
                            border-radius: 2px;
                        }
                        #tooltip tr td:nth-child(1){
                            width:50px;
                        }
                        #tooltip tr td:nth-child(2){
                            text-align:center;
                        }
                    `
                    }
                </style>
                <figcaption className="figure-caption text-center">
                    Hover over shaded states for details!
                </figcaption>
                <div id="tooltip"></div>
            </div>
        );
    }
}

export default StateInformationMap;