import React from 'react';
import { shallow } from 'enzyme';
import CitiesPopAndPoverty from './CitiesPopAndPoverty';

import { CityPopPoverty } from '../../tools/constants';


describe('City Population and Poverty', () => {
    // @Author Jorge
    it('renders correctly', () => {
        const wrapper = shallow( <CitiesPopAndPoverty
            width={540} height={540}
            data={CityPopPoverty}/>);
        
        expect(wrapper).toMatchSnapshot();
    });
});