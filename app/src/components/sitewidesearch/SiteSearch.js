import React, { Component } from 'react';
import Row from '../general/Row';
import Pagination from '../pagination/Pagination';
import { createCharityCardFn } from '../models/charities/CharitiesModel';
import { createEventCardFn } from '../models/events/EventsModel';
import { createCityCardFn } from '../models/cities/CitiesModel';

class SiteSearch extends Component {

    render() {
        const q = this.props.match.params.q;
        return (
            <div>
            <br/>
            <br/>
                <div className="container">
                    <Row columnType="col-lg-12 col-md-8">
                        <ul className="nav nav-tabs" id="resultsTab" role="tablist">
                        <li className="nav-item">
                            <a className="nav-link active"
                                id="charities-tab" data-toggle="tab"
                                href="#charities" role="tab" aria-controls="charities"
                                aria-selected="true">Charities</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link"
                                id="events-tab" data-toggle="tab"
                                href="#events" role="tab" aria-controls="events"
                                aria-selected="false">Events</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" id="cities-tab" data-toggle="tab"
                                href="#cities" role="tab" aria-controls="cities"
                                aria-selected="false">Cities</a>
                        </li>
                        </ul>
                    </Row>
                    <br/>
                    <Row columnType="col-lg-12 col-md-8">
                        <div className="tab-content" id="resultsTabContent">
                            <div className="tab-pane fade show active" id="charities"
                                role="tabpanel" aria-labelledby="charities-tab">
                            <Pagination
                                rowCount={5}
                                colCount={4}
                                apiEndpoint={`charities?q=${q}`}
                                cardFn={createCharityCardFn(q)}/>
                            </div>
                            <div className="tab-pane fade" id="events"
                                role="tabpanel" aria-labelledby="events-tab">
                                <Pagination
                                    rowCount={5}
                                    colCount={4}
                                    apiEndpoint={`events?q=${q}`}
                                    cardFn={createEventCardFn(q)}/>
                            </div>
                            <div className="tab-pane fade" id="cities"
                                role="tabpanel" aria-labelledby="cities-tab">
                                <Pagination
                                    rowCount={5}
                                    colCount={4}
                                    apiEndpoint={`cities?q=${q}`}
                                    cardFn={createCityCardFn(q)}/>
                            </div>
                        </div>
                    </Row>
                </div>
            </div>
        )
    }
}


export default SiteSearch;