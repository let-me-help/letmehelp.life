import React from 'react';
import { shallow } from 'enzyme';
import SiteSearch from './SiteSearch';

describe('SiteSearch', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<SiteSearch match={ {params: {q: "test"} } }/>);

        expect(wrapper).toMatchSnapshot();
    });
});