Daniel Loera dl29576 danielloera  
Estimated completion time: 10hr  
Actual completion time: 5hr  

Jorge Hernandez jmh6892 jmhern  
Estimated completion time: 5hr  
Actual completion time: 5hr  

Patrick Klingler plk395 pklingler  
Estimated completion time: 10hr  
Actual completion time: 8hr  

Rahul Parikh rmp2798 Rahul.Parikh  
Estimated completion time:5hr  
Actual completion time: 3hr  

Rinchen Tsering rt24459 rinchman  
Estimated completion time: 8hr  
Actual completion time: 8hr  

Git SHA: 150c167a04800fa7ca9605b9f6c2b62fe6af5148

[Pipelines](https://gitlab.com/let-me-help/letmehelp.life/pipelines) 
 
Website: [letmehelp.life](http://letmehelp.life)  
RESTful API: [api.letmehelp.life](http://api.letmehelp.life)

## letmehelp.life
Welcome to official repository for [letmehelp.life](http://letmehelp.life) front-end! 
Our mission is to provide an efficient, useful, and highly interactive website to discover charities near you.  

Use LetMeHelp to find the perfect charity for you, the easy way!

Comments:
-Our frontend tests are files that end in .test.js. They can be found on the same level as their corresponding component (all components are under `/app/src/component`). 
We felt having one large test.js file would be a giant mess, so this is why we broke down our tests this way.

-We have the gui tests written up, but unfortunately we were not able to add it to the pipeline in time. If you would like to run them simple go to `/guitests` and make sure you have selenium 
installed via pip and run `python guitests.py`.
