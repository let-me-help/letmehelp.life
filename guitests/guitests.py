import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options

def test_pagination_cards(driver, resource):
    nav = driver.find_element_by_link_text(resource)
    nav.click()
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.CLASS_NAME, "card-header"))
    )

    card = driver.find_element_by_class_name("card-header")
    
    link = card.find_element_by_tag_name("a")
    link.click()
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.TAG_NAME, "iframe"))
    )
       
  

def test_pagination(driver, resource):
    nav_button = driver.find_element_by_partial_link_text(resource)
    nav_button.click()

    # test first page load
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.CLASS_NAME, "card-header"))
    )
    cards = driver.find_elements_by_class_name("card-header")
    # test next page load
    load_more = driver.find_element(By.XPATH, '//button[text()="Load More"]')
    load_more.click()

    try:
        WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//button[text()="Load More"]'))
        )
        next_cards = driver.find_elements_by_class_name("card-header")
        assert len(next_cards) > len(cards)
    except TimeoutException:
        WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//button[text()="You\'ve reached the end!"]'))
        )
        next_cards = driver.find_elements_by_class_name("card-header")
        assert len(next_cards) == len(cards)

def check_navigation_to_resource(driver, resource):
    nav = driver.find_element_by_partial_link_text(resource)
    nav.click()
    assert "Browse " + resource in driver.page_source

def check_resource_search_tab(driver, resource):
    driver.find_element_by_id("{}-tab".format(resource)).click()
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.CLASS_NAME, "card-header"))
    )
    card_results = driver.find_elements_by_class_name("card-header")
    assert len(card_results) > 0

def check_advanced_options(driver, resource):
    nav = driver.find_element_by_partial_link_text(resource)
    nav.click()
    adv_opt = driver.find_element(By.XPATH, '//a[text()="Advanced Options"]')
    adv_opt.click()
    WebDriverWait(driver, 5).until(
        EC.presence_of_element_located((By.CLASS_NAME, "collapse"))
    )

class GuiTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = Options()
        options.set_headless(headless=True)
        cls.driver = webdriver.Firefox(firefox_options=options, executable_path='./geckodriver')
        cls.driver.get("http://letmehelp.life/")

    def test_title(self):
        assert "LetMeHelp" in self.driver.title

    def test_navigation(self):
        nav_elements_resource = ["Charities", "Events", "Cities"]
        for nav_elem in nav_elements_resource:
            check_navigation_to_resource(self.driver, nav_elem)

    def test_about(self):
        nav = self.driver.find_element_by_partial_link_text("About")
        nav.click()
        assert "About LetMeHelp" in self.driver.page_source

    def test_pagination_for_resources(self):
        for resource in ('Charities', 'Events', 'Cities'):
            test_pagination(self.driver, resource)

    def test_pagination_cards_for_resources(self):
        for resource in ('Charities', 'Events', 'Cities'):
            test_pagination_cards(self.driver, resource)
    
    def test_site_wide_search(self):
        search_form = self.driver.find_element_by_class_name("form-inline")
        search_input = search_form.find_element_by_tag_name("input")
        search_button = search_form.find_element_by_tag_name("a")

        search_input.send_keys("Texas")
        search_button.click()

        WebDriverWait(self.driver, 5).until(
            EC.presence_of_element_located((By.ID, 'resultsTab'))
        )

        for resource in ('charities', 'events', 'cities'):
            check_resource_search_tab(self.driver, resource)
    
    def test_advanced_options(self):
        # make sure filter, sort, search options show up
        for resource in ('Charities', 'Events', 'Cities'):
            check_advanced_options(self.driver, resource)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

if __name__ == "__main__":
    unittest.main()
